function setSessionUser(user)
{
    let encrypted = CryptoJS.AES.encrypt(JSON.stringify(user), SESSION_CONST);
    localStorage.setItem(SESSION_USER_KEY_CONST, encrypted);
}

function getSessionUser()
{
    let user = {};

    try
    {
        let encrypted = localStorage.getItem(SESSION_USER_KEY_CONST);
        let decrypted = CryptoJS.AES.decrypt(encrypted, SESSION_CONST);
        let plaintext = decrypted.toString(CryptoJS.enc.Utf8);
        user = JSON.parse(plaintext);
    }
    catch(ex)
    {
        console.error(ex);
    }

    return user;
}

function isLoggedIn()
{
    let user = {};

    try
    {
        let encrypted = localStorage.getItem(SESSION_USER_KEY_CONST);
        let decrypted = CryptoJS.AES.decrypt(encrypted, SESSION_CONST);
        let plaintext = decrypted.toString(CryptoJS.enc.Utf8);
        user = JSON.parse(plaintext);

        if(user != null && user.token && user.token != "")
        {
            return true;
        }

        return false;
    }
    catch(ex)
    {
        //console.error(ex);
        return false;
    }
    return false;
}

function logout()
{
    localStorage.removeItem(SESSION_USER_KEY_CONST);
    window.location.assign(LOGOUT_URL);
}