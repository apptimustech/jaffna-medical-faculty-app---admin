function toggleAsideBar(isToShow)
{
    if(isToShow)
    {
        $("body").addClass("aside-menu-lg-show aside-menu-show");
    }
    else
    {
        $("body").removeClass("aside-menu-lg-show aside-menu-show");
    }
}

function getURLParameter(parameter) {
    var pValue = "";
    try
    {
        var vars = window.location.search.split("?")[1].split("&");
        var query_string = {};
        for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        var key = decodeURIComponent(pair[0]);
        var value = decodeURIComponent(pair[1]);

        if (typeof query_string[key] === "undefined") {
            query_string[key] = decodeURIComponent(value);
            // If second entry with this name
        } else if (typeof query_string[key] === "string") {
            var arr = [query_string[key], decodeURIComponent(value)];
            query_string[key] = arr;
            // If third or later entry with this name
        } else {
            query_string[key].push(decodeURIComponent(value));
        }
        }

        //console.log(query_string);
        pValue = query_string[parameter];
        pValue = pValue ? pValue : "";
    }catch(err)
    {
        //console.error(err);
    }
    return pValue;
  }