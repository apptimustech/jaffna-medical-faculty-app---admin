function sendHttpRequest(url, method, data, callback, isFormData = false)
{
    var result = {};

    sessionUser = sessionUser ? sessionUser : {};
    
    data = data ? data : {};
    
    if(!isFormData)
        data.token  =  sessionUser.token || "";
    else 
        url += "&token=" + sessionUser.token || "";

    console.log(data);
    console.log(url);

    try
    {
        var ajaxOptions = {
            type: method,
            dataType: "json",
            url: url,
            crossDomain: true,
            timeout: 100000,
            data: data,
            success: function(response, textStatus ){
                result.data = response;
                result.is_success = true;
                result.msg = "Request was successful";

                console.log("REQUEST RESULT ========================= " + url);
                console.log(result);
                callback(result);
            },
            fail: function(xhr, textStatus, errorThrown){
                result.is_success = false;
                result.msg = "Sorry something went wrong. Please try again.";
                result.exception = errorThrown;
                console.log(xhr);
                callback(result);
            },
            error: function(xhr, textStatus) {
                result.is_success = false;
                result.msg = "Sorry something went wrong. Please try again.";
                console.log(xhr);
                callback(result);
            }
        };

        if(isFormData)
        {
            ajaxOptions.cache = false;
            ajaxOptions.processData = false;
            ajaxOptions.contentType = false;
        }

        $.ajax(ajaxOptions);
    }
    catch(err)
    {
        result.is_success = false;
        result.msg = "Sorry something went wrong. Please try again.";
        result.exception = err;
        //console.log(result);
        callback(result);
    }
}