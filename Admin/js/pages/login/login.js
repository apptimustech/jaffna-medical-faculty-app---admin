$(document).ready(function(){
    $("#loader").fadeOut();
});


//****************************************
//Login function
//****************************************
$("#btnLogin").click(function(){
    let user = {};
    user.user_name = $("#txt-uname").val();
    user.password = $("#txt-password").val();
    console.log(user);

    var url = API_BASE_URL + "/admin-login";
    sendHttpRequest(url, "POST", user, processResponse);
});

function processResponse(response)
{
    console.log(response);
    if(response.is_success)
    {
        if(response.data.is_success)
        {
            let user = response.data.user;
            user.token = response.data.token;
            setSessionUser(user);
            $("#msgBox").html("<b style='color:green'>Login successful. Redirecting...</b>");

            setTimeout(() => {
                window.location.assign("pages/users");
            }, 2000);
        }
        else
        {
            $("#msgBox").html("<b style='color:red'>"+ (response.data.msg || "Wrong username or password") +"</b>");
        }
    }
    else
    {
        $("#msgBox").html("<b style='color:red'>"+ (response.msg || "Wrong username or password") +"</b>");
    }
}