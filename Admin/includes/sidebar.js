const sidebarIncStr = `
    <nav class="sidebar-nav">
    <ul class="nav">
    <li class="nav-item">
        <a class="nav-link sidebar-link" id="link-contents" href="`+ PAGE_BASE_URL + `/pages/contents">
        <i class="nav-icon icon-drop"></i> Contents</a>
    </li>
    <li class="nav-item">
        <a class="nav-link sidebar-link" id="link-categories" href="`+ PAGE_BASE_URL + `/pages/categories">
        <i class="nav-icon icon-pencil"></i> Categories</a>
    </li>
    <li class="nav-item">
        <a class="nav-link sidebar-link" id="link-tags" href="`+ PAGE_BASE_URL + `/pages/tags">
        <i class="nav-icon icon-pencil"></i> Tags</a>
    </li>
    <li class="nav-item">
        <a class="nav-link sidebar-link" id="link-users" href="`+ PAGE_BASE_URL + `/pages/users">
        <i class="nav-icon icon-pencil"></i> Users</a>
    </li>

    <li class="nav-title" style="display:none">Reports</li>
    <li class="nav-item" style="display:none">
        <a class="nav-link sidebar-link" id="link-viewsreport" href="`+ PAGE_BASE_URL + `/pages/viewsreport">
        <i class="nav-icon icon-pencil"></i> Views Report</a>
    </li>
    <li class="nav-item" style="display:none">
        <a class="nav-link sidebar-link" id="link-visitsreport" href="`+ PAGE_BASE_URL + `/pages/visitsreport">
        <i class="nav-icon icon-pencil"></i> Visits Reports</a>
    </li>
    <li class="nav-item" style="display:none">
        <a class="nav-link sidebar-link" id="link-downloadreport" href="`+ PAGE_BASE_URL + `/pages/downloadreport">
        <i class="nav-icon icon-pencil"></i> Download Report</a>
    </li>
	<li class="nav-item">
        <a class="nav-link sidebar-link" id="link-downloadreport" onclick="logout()">
        <i class="nav-icon icon-lock"></i> Logout</a>
    </li>
    </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>`;