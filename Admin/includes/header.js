    const headerIncStr = `
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
    <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">
    <img class="navbar-brand-full" src="` + baseDirectory + `img/brand/logo.png" style="height:40px;border-radius:5px" alt="MEDI" >
    <img class="navbar-brand-minimized" src="` + baseDirectory + `img/brand/sygnet.svg" width="30" height="30" alt="MEDI">
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
    <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="nav navbar-nav ml-auto">
    <li class="nav-item">
        <a class="nav-link" href="#">
        <i class="fa fa-search" onclick="$('#searchBar').fadeIn()"></i>
        </a>
    </li>
    <li class="nav-item dropdown" style="margin-right:10px;display:none">
        <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
        <img class="img-avatar" src="` + baseDirectory + `img/avatars/logo.jpg" alt="admin@bootstrapmaster.com">
        </a>
        <div class="dropdown-menu dropdown-menu-right">
        <div class="dropdown-header text-center">
            <strong>Account</strong>
        </div>
        <a class="dropdown-item" href="#">
            <i class="fa fa-shield"></i> Lock Account</a>
        <a class="dropdown-item" href="#" onclick="logout()">
            <i class="fa fa-lock"></i> Logout</a>
        </div>
    </li>
    </ul>
    <button class="navbar-toggler aside-menu-toggler d-md-down-none" type="button" data-toggle="aside-menu-lg-show" style="display:none">
    <span class="navbar-toggler-icon"></span>
    </button>
    <button class="navbar-toggler aside-menu-toggler d-lg-none" type="button" data-toggle="aside-menu-show" style="display:none">
    <span class="navbar-toggler-icon"></span>
    </button>
    
    <div id="searchBar">
        <div class="search-panel">
            <div class="input-group">
                <input type="text" class="" placeholder="Search.." id="search-input"/>
                <i class="fa fa-close search-close" onclick="$('#searchBar').fadeOut();$('#search-input').focus()"></i>    
            </div>
        </div>
    </div>
    `;