/*
SQLyog Community v12.5.0 (64 bit)
MySQL - 5.7.20-log : Database - app_medical_faculty
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `attachments` */

DROP TABLE IF EXISTS `attachments`;

CREATE TABLE `attachments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `dp` longtext COLLATE utf8mb4_unicode_ci,
  `content_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_attachments_attachments1_idx` (`content_id`),
  CONSTRAINT `attachments_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `contents` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `attachments` */

insert  into `attachments`(`id`,`type`,`name`,`file`,`dp`,`content_id`,`created_at`,`updated_at`) values 
(1,'WORD','Medical.docx','medic.docx',NULL,1,NULL,NULL),
(2,'PDF','medical.pdf','medical.pdf',NULL,1,NULL,NULL),
(3,'PPT','medical.ppt','medical.ppt',NULL,1,NULL,NULL),
(4,'YOUTUBE VIDEO','medical.mp4','medical.mp4',NULL,1,NULL,NULL),
(5,'HYPERLINK','medical.pdf','medical.pdf',NULL,1,NULL,NULL),
(6,'FILE','medical.pdf','medical.pdf',NULL,1,NULL,NULL),
(7,'IMAGE',NULL,'1553595856.png',NULL,6,'2019-03-26 15:54:16','2019-03-26 15:54:16'),
(8,'VIDEO',NULL,'1553598632.mp4',NULL,6,'2019-03-26 16:40:32','2019-03-26 16:40:32'),
(9,'TEXT',NULL,'Any use of the Site, including any information and content provided by it or accessed through it, is at your own risk. Your use of the Site is not intended to and does not create a business, professional service, or other relationship between you and Zyom.\r\n\r\nThe information and content on the Site is for your personal, non-commercial use only. You may not modify the information or content in any way and you may not remove or alter any copyright, trademark, or other proprietary notice. Except where your use constitutes \"fair use\" under applicable copyright law, you may not use, download, upload, copy, reproduce, print, display, publish, license, transmit or otherwise distribute any information or content from the Site without the prior express written consent of Zyom.\r\n\r\nThe Site may contain links that provide access to other websites and services. These links are provided for your convenience and information. Use of any website or service accessed through the Site is at your own risk. The placement of these links on the Site does not mean that Zyom endorses or sponsors such websites or services, or that Zyom is affiliated with any third party operating or sponsoring such websites or services. Zyom is not responsible for and makes no warranties with respect to the websites or services accessed through this Site, nor any products or services purchased or obtained through such websites or services.',NULL,6,'2019-03-26 16:58:38','2019-03-26 16:58:38'),
(10,'FILE',NULL,'30104251553599956.pdf',NULL,6,'2019-03-26 17:02:36','2019-03-26 17:02:36'),
(11,'HTML',NULL,'<iframe width=\"100%\" height=\"400\" src=\"https://www.youtube.com/embed/8vAxdmb-Qkc\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>',NULL,6,'2019-03-26 17:03:28','2019-03-26 17:03:28');

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dp` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `fk_product_catagories_product_catagories1_idx` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `categories` */

insert  into `categories`(`id`,`name`,`description`,`parent_id`,`created_at`,`updated_at`,`dp`) values 
(2,'Tutorials','Tutorials Description',NULL,'2018-11-06 11:43:13','2018-11-06 11:43:13','DEFAULT-CONTENT-BG.jpg'),
(3,'Video Links','Youtube',NULL,'2018-11-06 12:44:35','2018-11-06 14:40:31','DEFAULT-CONTENT-BG.jpg'),
(4,'Hyperlinks','links',NULL,'2018-11-06 12:46:15','2018-11-06 14:34:18','DEFAULT-CONTENT-BG.jpg'),
(5,'Medical','Medical',2,'2018-11-06 13:32:42','2018-11-06 14:39:42','DEFAULT-CONTENT-BG.jpg'),
(7,'Cancer Related','Dummy',5,'2018-11-06 14:14:35','2018-11-06 14:14:35','DEFAULT-CONTENT-BG.jpg'),
(8,'Dummy','Dum',7,'2018-11-06 14:14:54','2018-11-06 14:14:54','DEFAULT-CONTENT-BG.jpg'),
(9,'Sample','Sample',3,'2018-11-07 09:45:16','2018-11-07 09:45:16','DEFAULT-CONTENT-BG.jpg'),
(10,'Test','Test',9,'2018-11-07 09:45:29','2018-11-07 09:45:29','DEFAULT-CONTENT-BG.jpg'),
(11,'Test2','DEFAULT-CONTENT-BG.jpg',NULL,'2019-03-26 20:03:32','2019-03-26 20:03:32','CATEGORY_DP_18356961553610812.ico');

/*Table structure for table `content_has_tags` */

DROP TABLE IF EXISTS `content_has_tags`;

CREATE TABLE `content_has_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `contents_id` int(11) NOT NULL,
  `tags_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `content_has_tags` */

insert  into `content_has_tags`(`id`,`contents_id`,`tags_id`,`created_at`,`updated_at`) values 
(1,1,3,NULL,NULL),
(2,1,7,NULL,NULL),
(3,6,3,'2019-03-26 13:40:15','2019-03-26 13:40:15'),
(4,6,7,'2019-03-26 13:41:03','2019-03-26 13:41:03'),
(5,6,9,'2019-03-26 13:42:02','2019-03-26 13:42:02'),
(6,6,10,'2019-03-26 14:09:39','2019-03-26 14:09:39'),
(7,6,10,'2019-03-26 17:06:55','2019-03-26 17:06:55'),
(8,6,11,'2019-03-26 17:07:12','2019-03-26 17:07:12'),
(9,6,12,'2019-03-26 17:07:32','2019-03-26 17:07:32');

/*Table structure for table `contents` */

DROP TABLE IF EXISTS `contents`;

CREATE TABLE `contents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci,
  `category_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_contents_categories1_idx` (`category_id`),
  CONSTRAINT `fk_contents_categories1_idx` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `contents` */

insert  into `contents`(`id`,`title`,`body`,`category_id`,`created_at`,`updated_at`) values 
(1,'Test','Some dummy text to explain',3,NULL,NULL),
(3,'Test Content','Just dummy description',2,'2019-03-26 06:52:41','2019-03-26 06:52:41'),
(4,'Test 2','Test body',3,'2019-03-26 12:18:40','2019-03-26 12:18:40'),
(5,'Test 2 3','Test body',3,'2019-03-26 12:21:07','2019-03-26 12:21:07'),
(6,'ddd2','grergrg',3,'2019-03-26 12:23:01','2019-03-26 13:44:49');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1),
(3,'2016_06_01_000001_create_oauth_auth_codes_table',1),
(4,'2016_06_01_000002_create_oauth_access_tokens_table',1),
(5,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),
(6,'2016_06_01_000004_create_oauth_clients_table',1),
(7,'2016_06_01_000005_create_oauth_personal_access_clients_table',1),
(8,'2018_10_08_093720_create_categories_table',1),
(9,'2018_10_08_093840_create_contents_table',1),
(10,'2018_10_08_093937_create_attachments_table',1),
(11,'2018_10_08_094018_create_user_impressions_table',1),
(12,'2018_10_08_094042_create_tags_table',1),
(13,'2018_10_09_055419_create_content_has_tags_table',1);

/*Table structure for table `oauth_access_tokens` */

DROP TABLE IF EXISTS `oauth_access_tokens`;

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_access_tokens` */

/*Table structure for table `oauth_auth_codes` */

DROP TABLE IF EXISTS `oauth_auth_codes`;

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_auth_codes` */

/*Table structure for table `oauth_clients` */

DROP TABLE IF EXISTS `oauth_clients`;

CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_clients` */

/*Table structure for table `oauth_personal_access_clients` */

DROP TABLE IF EXISTS `oauth_personal_access_clients`;

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_personal_access_clients` */

/*Table structure for table `oauth_refresh_tokens` */

DROP TABLE IF EXISTS `oauth_refresh_tokens`;

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `oauth_refresh_tokens` */

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `tags` */

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `tags` */

insert  into `tags`(`id`,`name`,`created_at`,`updated_at`) values 
(3,'Youtube','2018-11-06 14:49:15','2018-11-06 14:49:15'),
(7,'Link',NULL,NULL),
(8,'heart','2019-03-22 17:44:07','2019-03-22 17:44:07'),
(9,'Test Tag','2019-03-26 13:41:33','2019-03-26 13:41:33'),
(10,'Hello','2019-03-26 14:09:39','2019-03-26 14:09:39'),
(11,'medical faculty','2019-03-26 17:07:11','2019-03-26 17:07:11'),
(12,'exhibition','2019-03-26 17:07:32','2019-03-26 17:07:32');

/*Table structure for table `user_impressions` */

DROP TABLE IF EXISTS `user_impressions`;

CREATE TABLE `user_impressions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_impressions_user_impressions1_idx` (`content_id`),
  CONSTRAINT `user_impressions_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `contents` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `user_impressions` */

insert  into `user_impressions`(`id`,`type`,`value`,`ip`,`content_id`,`created_at`,`updated_at`) values 
(1,'view','1','127.0.0.1',1,'2018-11-06 23:35:09','2018-11-06 23:35:09'),
(2,'view','1','127.0.0.1',1,'2018-11-06 23:35:19','2018-11-06 23:35:19'),
(3,'view','1','127.0.0.1',1,'2018-11-06 23:36:55','2018-11-06 23:36:55'),
(4,'view','1','127.0.0.1',1,'2018-11-06 23:38:00','2018-11-06 23:38:00'),
(5,'view','1','127.0.0.1',6,'2019-03-26 12:23:06','2019-03-26 12:23:06'),
(6,'view','1','127.0.0.1',6,'2019-03-26 12:23:52','2019-03-26 12:23:52'),
(7,'view','1','127.0.0.1',6,'2019-03-26 12:25:09','2019-03-26 12:25:09'),
(8,'view','1','127.0.0.1',6,'2019-03-26 12:26:07','2019-03-26 12:26:07'),
(9,'view','1','127.0.0.1',6,'2019-03-26 12:47:18','2019-03-26 12:47:18'),
(10,'view','1','127.0.0.1',6,'2019-03-26 12:47:52','2019-03-26 12:47:52'),
(11,'view','1','127.0.0.1',6,'2019-03-26 12:48:35','2019-03-26 12:48:35'),
(12,'view','1','127.0.0.1',6,'2019-03-26 12:49:15','2019-03-26 12:49:15'),
(13,'view','1','127.0.0.1',6,'2019-03-26 12:49:37','2019-03-26 12:49:37'),
(14,'view','1','127.0.0.1',6,'2019-03-26 13:33:27','2019-03-26 13:33:27'),
(15,'view','1','127.0.0.1',6,'2019-03-26 13:35:01','2019-03-26 13:35:01'),
(16,'view','1','127.0.0.1',6,'2019-03-26 13:35:50','2019-03-26 13:35:50'),
(17,'view','1','127.0.0.1',6,'2019-03-26 13:36:26','2019-03-26 13:36:26'),
(18,'view','1','127.0.0.1',6,'2019-03-26 13:37:22','2019-03-26 13:37:22'),
(19,'view','1','127.0.0.1',6,'2019-03-26 13:38:32','2019-03-26 13:38:32'),
(20,'view','1','127.0.0.1',6,'2019-03-26 13:39:39','2019-03-26 13:39:39'),
(21,'view','1','127.0.0.1',6,'2019-03-26 13:40:12','2019-03-26 13:40:12'),
(22,'view','1','127.0.0.1',6,'2019-03-26 13:40:36','2019-03-26 13:40:36'),
(23,'view','1','127.0.0.1',6,'2019-03-26 13:40:59','2019-03-26 13:40:59'),
(24,'view','1','127.0.0.1',6,'2019-03-26 13:41:55','2019-03-26 13:41:55'),
(25,'view','1','127.0.0.1',6,'2019-03-26 13:42:09','2019-03-26 13:42:09'),
(26,'view','1','127.0.0.1',6,'2019-03-26 13:42:33','2019-03-26 13:42:33'),
(27,'view','1','127.0.0.1',6,'2019-03-26 13:44:46','2019-03-26 13:44:46'),
(28,'view','1','127.0.0.1',6,'2019-03-26 13:45:14','2019-03-26 13:45:14'),
(29,'view','1','127.0.0.1',6,'2019-03-26 13:55:02','2019-03-26 13:55:02'),
(30,'view','1','127.0.0.1',6,'2019-03-26 13:55:55','2019-03-26 13:55:55'),
(31,'view','1','127.0.0.1',6,'2019-03-26 13:56:09','2019-03-26 13:56:09'),
(32,'view','1','127.0.0.1',6,'2019-03-26 14:01:41','2019-03-26 14:01:41'),
(33,'view','1','127.0.0.1',6,'2019-03-26 14:02:38','2019-03-26 14:02:38'),
(34,'view','1','127.0.0.1',6,'2019-03-26 14:03:15','2019-03-26 14:03:15'),
(35,'view','1','127.0.0.1',6,'2019-03-26 14:03:39','2019-03-26 14:03:39'),
(36,'view','1','127.0.0.1',6,'2019-03-26 14:05:12','2019-03-26 14:05:12'),
(37,'view','1','127.0.0.1',6,'2019-03-26 14:08:25','2019-03-26 14:08:25'),
(38,'view','1','127.0.0.1',6,'2019-03-26 14:08:39','2019-03-26 14:08:39'),
(39,'view','1','127.0.0.1',6,'2019-03-26 14:09:14','2019-03-26 14:09:14'),
(40,'view','1','127.0.0.1',6,'2019-03-26 14:09:43','2019-03-26 14:09:43'),
(41,'view','1','127.0.0.1',6,'2019-03-26 15:23:04','2019-03-26 15:23:04'),
(42,'view','1','127.0.0.1',6,'2019-03-26 15:27:29','2019-03-26 15:27:29'),
(43,'view','1','127.0.0.1',6,'2019-03-26 15:28:27','2019-03-26 15:28:27'),
(44,'view','1','127.0.0.1',6,'2019-03-26 15:34:01','2019-03-26 15:34:01'),
(45,'view','1','127.0.0.1',6,'2019-03-26 15:35:50','2019-03-26 15:35:50'),
(46,'view','1','127.0.0.1',6,'2019-03-26 15:40:10','2019-03-26 15:40:10'),
(47,'view','1','127.0.0.1',6,'2019-03-26 15:54:05','2019-03-26 15:54:05'),
(48,'view','1','127.0.0.1',6,'2019-03-26 16:11:09','2019-03-26 16:11:09'),
(49,'view','1','127.0.0.1',6,'2019-03-26 16:11:56','2019-03-26 16:11:56'),
(50,'view','1','127.0.0.1',6,'2019-03-26 16:12:54','2019-03-26 16:12:54'),
(51,'view','1','127.0.0.1',6,'2019-03-26 16:19:43','2019-03-26 16:19:43'),
(52,'view','1','127.0.0.1',6,'2019-03-26 16:35:15','2019-03-26 16:35:15'),
(53,'view','1','127.0.0.1',6,'2019-03-26 16:35:21','2019-03-26 16:35:21'),
(54,'view','1','127.0.0.1',6,'2019-03-26 16:38:14','2019-03-26 16:38:14'),
(55,'view','1','127.0.0.1',6,'2019-03-26 16:39:43','2019-03-26 16:39:43'),
(56,'view','1','127.0.0.1',6,'2019-03-26 16:43:31','2019-03-26 16:43:31'),
(57,'view','1','127.0.0.1',6,'2019-03-26 16:46:37','2019-03-26 16:46:37'),
(58,'view','1','127.0.0.1',6,'2019-03-26 16:47:48','2019-03-26 16:47:48'),
(59,'view','1','127.0.0.1',6,'2019-03-26 16:49:50','2019-03-26 16:49:50'),
(60,'view','1','127.0.0.1',6,'2019-03-26 16:50:19','2019-03-26 16:50:19'),
(61,'view','1','127.0.0.1',6,'2019-03-26 16:51:40','2019-03-26 16:51:40'),
(62,'view','1','127.0.0.1',6,'2019-03-26 16:52:25','2019-03-26 16:52:25'),
(63,'view','1','127.0.0.1',6,'2019-03-26 16:53:18','2019-03-26 16:53:18'),
(64,'view','1','127.0.0.1',6,'2019-03-26 16:54:34','2019-03-26 16:54:34'),
(65,'view','1','127.0.0.1',6,'2019-03-26 16:54:51','2019-03-26 16:54:51'),
(66,'view','1','127.0.0.1',6,'2019-03-26 16:59:16','2019-03-26 16:59:16'),
(67,'view','1','127.0.0.1',6,'2019-03-26 16:59:40','2019-03-26 16:59:40'),
(68,'view','1','127.0.0.1',6,'2019-03-26 17:00:31','2019-03-26 17:00:31'),
(69,'view','1','127.0.0.1',6,'2019-03-26 17:00:55','2019-03-26 17:00:55'),
(70,'view','1','127.0.0.1',6,'2019-03-26 17:02:14','2019-03-26 17:02:14'),
(71,'view','1','127.0.0.1',6,'2019-03-26 17:04:24','2019-03-26 17:04:24'),
(72,'view','1','127.0.0.1',6,'2019-03-26 17:04:31','2019-03-26 17:04:31'),
(73,'view','1','127.0.0.1',6,'2019-03-26 17:05:14','2019-03-26 17:05:14'),
(74,'view','1','127.0.0.1',6,'2019-03-26 17:05:42','2019-03-26 17:05:42'),
(75,'view','1','127.0.0.1',6,'2019-03-26 17:06:12','2019-03-26 17:06:12'),
(76,'view','1','127.0.0.1',6,'2019-03-26 17:09:00','2019-03-26 17:09:00'),
(77,'view','1','127.0.0.1',6,'2019-03-26 17:09:49','2019-03-26 17:09:49'),
(78,'view','1','127.0.0.1',6,'2019-03-26 18:44:03','2019-03-26 18:44:03'),
(79,'view','1','127.0.0.1',6,'2019-03-26 18:44:03','2019-03-26 18:44:03'),
(80,'view','1','127.0.0.1',6,'2019-03-26 20:28:08','2019-03-26 20:28:08'),
(81,'view','1','127.0.0.1',1,'2019-03-27 05:16:15','2019-03-27 05:16:15'),
(82,'view','1','127.0.0.1',6,'2019-03-27 05:16:22','2019-03-27 05:16:22'),
(83,'view','1','127.0.0.1',5,'2019-03-27 05:16:30','2019-03-27 05:16:30');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `token` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_user_name_unique` (`user_name`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=206 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`contact`,`user_name`,`email`,`email_verified_at`,`password`,`is_deleted`,`token`,`created_at`,`updated_at`) values 
(1,'Anojan Kaneshathas','0776122826','anoo','anoo@ano.com','2018-11-07 08:00:56','aaa',0,'gxSfxd8aZwFx027K3NxfW1YONl1R6a',NULL,'2019-03-25 10:26:36'),
(3,'Manojan Kaneshathas','94776122825','aaaa4','manoop@gmail.com',NULL,'$2y$10$ZXBd6YxYosev9IeWm0uqSebhmWe1kkz8GnCmwr5wfCUHaYO26.dmy',0,'Qlux08w7YiQidpw1H2xVn508C6rhXQ','2018-11-06 09:13:44','2018-11-06 09:13:44'),
(4,'Abinaya Dhanasekaran','024578963','abiii','abi@gmail.com',NULL,'$2y$10$.nTXJrydqpBC9EfjbDaJV.q5fZLw8ZHvPGR3p0CFm7I2I9ICm4jJq',1,'FRFS888SwotkmDhsKg2ZXkPLnl2OIO','2018-11-06 09:16:39','2018-11-07 09:42:34'),
(5,'David Balasubramaniam','0564','david','david@gmail.com',NULL,'$2y$10$xMJwtecS5qeh.ausKwHs8.d1WMZJ/7Z7Wwy5H9PvUKxBsjb6zj75G',0,'HMAX0GaorkQ9gX6Mvy7Us18vqtZ3Fg','2018-11-06 10:53:37','2018-11-06 10:53:37'),
(6,'Walter Downs','(624) 721-1226','Kalia','non.sapien@laoreetipsumCurabitur.com',NULL,'22725282-3CCF-D08F-AEC2-4D0907697895',0,'2E856EFE-0EF0-EF5B-7372-BB234414FF5E',NULL,NULL),
(7,'Fritz Lynn','(977) 917-9640','Lamar','nunc@commodotincidunt.edu',NULL,'E539B480-2A9E-6B80-2DDB-93243C0F954F',0,'5EBD187B-6BE0-CD1F-5897-069B310066C8',NULL,NULL),
(8,'Louis Rivers','(714) 801-0731','Ima','ipsum.Phasellus@elementumpurusaccumsan.net',NULL,'20793FC8-A0A7-C531-319C-1CD8BE28AA3E',0,'AE62C58B-F626-F08B-8646-5EAAA3CA735A',NULL,NULL),
(9,'Leroy Houston','(539) 491-6522','Rigel','nulla@tristiqueneque.ca',NULL,'E7407EE0-6D56-8E25-7E9E-4AF897617A12',0,'C206335B-FF9B-FCB7-6321-320E7D74B7DD',NULL,NULL),
(10,'Ferdinand Benjamin','(431) 805-5169','Unity','sed.pede.nec@tellusPhasellus.edu',NULL,'6B39811D-C93E-DEC5-F9BC-F83CEDB821D2',0,'CEA10B4A-D4D1-E1EB-9A68-3C9312624B47',NULL,NULL),
(11,'Robert Woodward','(443) 281-1882','Lester','Curabitur.massa.Vestibulum@eratneque.net',NULL,'111DB0BC-8399-634B-0457-226E54A3072B',0,'3CE2BA4D-0196-4931-FD3E-FDFF2B4ADB03',NULL,NULL),
(12,'Zeus Miranda','(576) 552-7081','Otto','et.ultrices@lobortisauguescelerisque.co.uk',NULL,'05502E2B-EF08-9B86-229B-3D51B198998C',0,'31E32F3F-7F68-C569-F601-AFDEA10BD4E4',NULL,NULL),
(13,'Omar Salazar','(585) 999-1293','Leandra','ante@luctusetultrices.com',NULL,'32013B97-B536-51A1-FEBB-97D6E203EB25',0,'53BFA75F-A198-38BC-0FD5-F6F2D3B87F6A',NULL,NULL),
(14,'Kareem Valdez','(830) 129-6657','Jessica','Cras.vulputate.velit@eratsemperrutrum.net',NULL,'408FE236-405C-42B4-639E-AFD90F469FE9',0,'0A1B12AB-B052-00AA-BC97-401C77B615AE',NULL,NULL),
(15,'Guy Bird','(837) 351-5128','Charissa','vel.faucibus.id@auctor.com',NULL,'F5E209CA-70CE-51B3-D95B-CAEB18567103',0,'D50CF7F3-7C96-4F86-26C0-52CB694ED850',NULL,NULL),
(16,'Basil Slater','(336) 404-4279','Abra','a.scelerisque@iaculisquis.edu',NULL,'8D8E7352-9B4C-0E92-3C38-112AB85633F2',0,'14B1B1F5-5FAF-DBD1-E43C-2E941B54DD56',NULL,NULL),
(17,'Thane Barlow','(407) 356-4844','Raymond','Vivamus.sit.amet@eleifendCras.org',NULL,'C779E509-327F-A7FE-D0EB-3168504EA942',0,'9B012D45-612B-8152-8E14-B0B6835FC9A7',NULL,NULL),
(18,'Bradley Schmidt','(388) 694-7891','Ina','nec.orci.Donec@magnisdisparturient.org',NULL,'D873E8BE-3C72-FD9A-2FCF-98195165E4AA',0,'D33A6732-9250-EB75-3ABA-6E043FFECB2C',NULL,NULL),
(19,'Samuel Paul','(368) 530-5910','Jelani','in.consequat.enim@orciconsectetuer.edu',NULL,'23D9D526-5D8E-43F4-C6D1-23AC64B39974',0,'FBD7B9E1-CA30-786B-A7DA-54585BF2C933',NULL,NULL),
(20,'Silas Mendez','(952) 811-4668','Raya','neque@risusvariusorci.com',NULL,'96FEC497-2CF5-528E-8D1B-3F0EDE14437F',0,'96309F15-584C-FC0A-AADA-DA3F06EF471E',NULL,NULL),
(21,'Upton Garrison','(518) 420-6755','Isaac','neque@vitaenibhDonec.org',NULL,'1EB8C4D5-211C-EC11-288E-DA9BAEF34B7C',0,'70563D93-DE4B-2C6D-052B-9C9F5F571364',NULL,NULL),
(22,'Prescott Whitfield','(685) 482-9054','Amir','tincidunt.congue.turpis@sempercursusInteger.edu',NULL,'A93440CE-4413-7D27-2DDD-AA905F9F63FC',0,'D2220CFE-2D6D-FBC7-773C-CCAAFFA943AD',NULL,NULL),
(23,'Wang Marks','(494) 612-9050','Lacey','Suspendisse.eleifend@Cumsociis.org',NULL,'082F7381-A449-A6AD-4C3F-972299489BD1',0,'96E6FB27-2FDC-6C18-3CCC-7E22FDC5A69D',NULL,NULL),
(24,'Hamilton Dominguez','(154) 289-3714','Baker','eget@Donec.ca',NULL,'6030A6A6-2033-DA5E-0B31-C313D5DCC96F',0,'0E0B5900-9520-E78B-E4C8-E276DB2DF081',NULL,NULL),
(25,'Guy Gillespie','(927) 895-0283','Sawyer','Ut.semper@Etiam.ca',NULL,'C993B348-0E26-6F81-330D-0F275F35A6E2',0,'8B5B73DC-427A-2392-6579-2EFA4E310658',NULL,NULL),
(36,'Malik Maxwell','(201) 276-1219','Paki','nunc@mus.ca',NULL,'F1498552-AF3C-014A-5A0F-C59DCF363A49',0,'0BB0CBAB-E720-F5F5-2196-ED206DA3A09B',NULL,NULL),
(37,'Edward Wise','(783) 437-3378','Justin','at.pretium@loremvehiculaet.edu',NULL,'8D5B81B6-1F5B-2D2C-E436-4F8342774C0C',0,'7EEEB86F-08D2-873F-EBF4-F71918A924B0',NULL,NULL),
(38,'Rudyard Mcdowell','(980) 541-0356','Mannix','a.auctor.non@penatibus.co.uk',NULL,'BC1CEA69-DFB0-F27E-B5F7-E9724C2D8315',0,'887CCDAC-21CB-24FE-81A9-7C48D4615B30',NULL,NULL),
(39,'Eric Reilly','(790) 489-8731','Rowan','tellus@eliteratvitae.net',NULL,'0F5F523B-BAC2-DB04-280A-BB11F520F522',0,'4B8FCCB7-962A-EB72-C6D3-CD513DAED2EC',NULL,NULL),
(40,'Oscar Rice','(537) 426-1592','Ali','malesuada.vel@orciluctuset.co.uk',NULL,'2FEDFE7F-A6F6-50EE-E0D0-6F8082B6E493',0,'0DFDB142-C304-9361-54D9-E68095B0FA0D',NULL,NULL),
(41,'Colby Hall','(349) 749-3201','Cherokee','Cras@velconvallis.edu',NULL,'5C67A4D7-ABED-AF62-C1B0-149EB379853C',0,'77A36AFE-9F3F-5114-0E22-6FBAD1BF3088',NULL,NULL),
(42,'Isaac Leblanc','(460) 563-2357','Amela','pretium.neque@ridiculusmus.ca',NULL,'27B0F172-96F2-AEA3-855C-791D6EA39B99',0,'F9FE3BC8-D645-492E-2EA2-B709055240C2',NULL,NULL),
(43,'Hall Love','(908) 775-0985','Myles','Nullam.vitae@placeratvelit.com',NULL,'5E9AFBED-F787-819B-00EE-914B5A59DE4B',0,'CC1FFB01-14F3-842A-BC42-6C93B9DA21D3',NULL,NULL),
(44,'Vaughan Acevedo','(677) 648-6731','Melvin','justo.eu.arcu@Proinvel.net',NULL,'03A55D7C-0C9F-7C5C-D560-56AE7A31877B',0,'67387F91-7734-7FD7-47AD-D36C2F21A6EF',NULL,NULL),
(45,'Davis Alford','(546) 623-0999','Herrod','velit@CuraePhasellus.co.uk',NULL,'A7898E9D-FCF5-4B93-303A-24EE39B2F289',0,'A97B0BE8-FA71-3179-70C9-E52F6E746464',NULL,NULL),
(46,'Charles Lindsay','(970) 813-7923','Blake','aliquam.adipiscing@metussit.org',NULL,'2D2195F6-AEBB-2A61-85F1-7AE057E7F1FE',0,'5C47F4A8-B6F9-9242-BA57-392BA4E3707A',NULL,NULL),
(47,'Price Douglas','(865) 416-2544','Tatiana','Pellentesque@nisisem.co.uk',NULL,'30D3E3A3-4EE0-8882-AD4B-5B9A31886C54',0,'686B3A4D-AB54-B040-D778-F0AD3E5883C7',NULL,NULL),
(48,'Keane Camacho','(326) 222-2280','Jamal','amet.consectetuer.adipiscing@orcitinciduntadipiscing.edu',NULL,'F5B245B6-D733-FA32-3405-F18CD957DB14',0,'B14FDF76-292B-4A71-D04E-25A7556DE8C3',NULL,NULL),
(49,'Marshall Sargent','(537) 757-6754','Amery','non.leo@massa.edu',NULL,'727E206A-1FE2-BF5D-154E-14F1E11EA4A9',0,'17888E92-757F-F393-DE4B-6E93540E0A9D',NULL,NULL),
(50,'Colt Simon','(821) 287-2900','Sheila','rutrum.magna.Cras@atiaculisquis.edu',NULL,'26CF4BA2-732F-B41B-59F7-C311A16416DD',0,'A5F9D74C-2ABC-83EC-7B0A-9E0F1814012D',NULL,NULL),
(51,'Chaim Hendricks','(456) 887-1463','Tanisha','Nulla.semper@nonummy.edu',NULL,'9A388573-FD55-F3D0-EEF1-A9BF47E86347',0,'930E2994-E060-96C4-B131-10F585D036F5',NULL,NULL),
(52,'Kevin Holt','(256) 631-6094','Julie','quis.diam@dictumaugue.edu',NULL,'211DE68D-29E7-3E3E-256C-41A74E028934',0,'51D2BD65-C887-7036-31A8-6F2A5881B25E',NULL,NULL),
(53,'Emerson Mitchell','(444) 345-4503','Jessamine','aliquam@sociosqu.edu',NULL,'2A84D7B9-934C-8497-5C55-C00F05F12120',0,'41644672-60C7-9FA4-239F-C609E2BBA214',NULL,NULL),
(54,'Aaron Banks','(457) 467-0183','Macey','Pellentesque@purussapiengravida.org',NULL,'00ADBAE0-08DE-60B0-3918-060318FC4E05',1,'8D5B4092-8F8A-D4DB-4B92-E60E3D5EAA40',NULL,'2018-11-06 11:22:18'),
(55,'David Branch','(624) 741-1977','Melyssa','Vivamus.molestie@Donec.net',NULL,'57A282A3-E832-9CAF-0FB6-FD73EC4969B7',0,'1DB44DF6-CA52-79EB-F4C2-D1D68CA3046F',NULL,NULL),
(66,'Kuame Noel','(217) 463-1722','Juliet','ultrices.posuere.cubilia@gravidanuncsed.co.uk',NULL,'7BFC4225-E8ED-7EB3-A209-65296178113D',0,'A0418D9A-DE8C-B4F3-5B9D-A179B0FA30D8',NULL,NULL),
(67,'Hashim Logan','(964) 660-0286','Raja','imperdiet.non@sociisnatoque.com',NULL,'2AA7A455-0DB7-43CA-E6D5-2E1C3A6DB165',0,'D0CDE96F-27B1-78B6-2120-986AFAD8B4D2',NULL,NULL),
(68,'Hashim Fowler','(332) 237-2129','Brock','in@eget.com',NULL,'9873EDFD-1CE2-1142-7E11-77D97F7904C6',0,'497732F9-A92A-93DE-BF56-3A3C5929AECB',NULL,NULL),
(69,'Dante Joyce','(113) 388-5673','Hiroko','lectus.ante.dictum@molestiesodalesMauris.net',NULL,'E0C98B02-AE90-CD79-C13C-1A7F6C598D09',0,'F49ABD8D-BFA7-0193-AF0E-D459C8270168',NULL,NULL),
(70,'Jackson Mcconnell','(817) 263-0760','Griffin','risus.In@adipiscing.ca',NULL,'22E7F87A-AA45-4F11-0F8F-37D558F33793',0,'28AF7108-0E9E-63EF-3D6C-E88E95B4150C',NULL,NULL),
(71,'Thor Gay','(194) 609-8308','Quentin','pede.blandit.congue@in.net',NULL,'3CAC09E4-28AC-A4F1-4089-DE7E48FECA13',0,'486AA90E-6F62-CA33-E980-88F7A93B7940',NULL,NULL),
(72,'Tyrone Mccarty','(603) 251-5495','Leslie','dapibus.rutrum@antedictummi.org',NULL,'4DB80AD9-36F9-0AC6-94BB-1C1BD20E6C53',0,'13A89D3D-313A-74FA-B077-5FB0CAF9B478',NULL,NULL),
(73,'Chaney Walton','(665) 897-6549','Evangeline','sem.Pellentesque@mauris.ca',NULL,'BF2D7150-E0D0-89E4-3249-E569B6F2CE26',0,'545AA878-ED5F-AC91-0A83-E218DFF2BA19',NULL,NULL),
(74,'Honorato Hansen','(391) 377-1642','Brady','sed@natoquepenatibuset.com',NULL,'786CD200-C2B9-C894-32F5-59C473D9BC5A',0,'9FE2756B-30DD-DD62-52EE-86D3B0F741C3',NULL,NULL),
(75,'Griffith Rosa','(959) 999-0104','Jackson','velit@Etiam.com',NULL,'737DB6D4-AFBC-F303-3D22-16D82A5DF413',0,'D1F814F1-DC87-B453-F6F5-8A03323C7AD1',NULL,NULL),
(76,'Alec Franks','(800) 917-7556','Angelica','pulvinar@montesnascetur.com',NULL,'F819B95B-ED0C-852F-6D25-B44AC82DD9DC',1,'880A1CC2-66A1-30F4-3586-01C1039FB924',NULL,'2018-11-06 11:26:35'),
(77,'Thor Forbes','(122) 214-4378','Felicia','velit.in@Vestibulum.com',NULL,'46229276-0D8C-65C8-5CB9-09B12E3AF0D7',0,'933EB33A-404A-D01A-B9BF-50DEC28ABF52',NULL,NULL),
(78,'Hedley Miranda','(548) 868-3800','Rafael','odio.a@consectetuercursuset.ca',NULL,'40216DAF-537E-D199-AB63-62A535DE6AAD',0,'736A897A-DF2E-5211-2BC3-50EF9BE4C6C7',NULL,NULL),
(79,'Wallace Velez','(376) 733-3536','Walker','sit@mi.ca',NULL,'9A55CD2B-14C9-F0C9-C1F4-0C4B6F4A75A0',0,'93D305BB-9B1A-C0D0-C83D-4A9BA8CC545D',NULL,NULL),
(80,'Tiger Bowers','(401) 116-8135','Marvin','feugiat.placerat@DonecfringillaDonec.ca',NULL,'85CF02C1-3C17-7781-653E-CB4F39B40531',0,'A8F981A6-8436-E15B-BC3E-8D678857C222',NULL,NULL),
(81,'Jackson Brewer','(293) 209-3085','India','vulputate@posuereatvelit.com',NULL,'ED58971E-663E-C417-F5BD-04DDD871E7F9',0,'4755A050-91AA-DFB5-C0BE-2CFA046160B1',NULL,NULL),
(82,'Julian Blevins','(233) 101-5822','Cheryl','cursus.in@nullamagnamalesuada.net',NULL,'E84D895F-796E-828E-AC2B-2C711B509800',0,'E2D7B504-1E0E-9004-DCC9-15F8335F70A7',NULL,NULL),
(83,'Avram Deleon','(843) 596-5437','Hedley','id.ante.dictum@enim.edu',NULL,'15D209E3-3994-5F14-E75F-9462A8185580',0,'91D921EC-98FC-CF1F-63DF-2C1CC70787EF',NULL,NULL),
(84,'Allen Gay','(168) 201-2106','Penelope','erat.Vivamus@magna.org',NULL,'676F1B3F-1F5D-2A8B-B93A-8EB236DE516A',0,'4280E736-7C71-4AC8-76C0-F152157705BA',NULL,NULL),
(85,'Noble Glover','(272) 886-2560','Selma','Suspendisse.dui@aliquetProin.edu',NULL,'D64D5E41-A992-947F-2616-5B17017B3134',0,'AC4AF933-3DDA-619D-1B62-1E3CE41D327C',NULL,NULL),
(86,'Ulysses Hoffman','(256) 908-6144','Stuart','dui.nec@dictum.org',NULL,'45D307C3-2DC0-A7C8-2009-291E99C216D6',0,'04473DDC-F821-3097-80C4-CB57F68A7C13',NULL,NULL),
(87,'Ryan Alford','(962) 940-9900','Veronica','aliquet.odio@eros.net',NULL,'510CBE64-3B95-4395-2371-6D06F2DEF995',0,'585A33EE-09C5-DA8E-0A47-08AC5E88173D',NULL,NULL),
(88,'Mark Edwards','(770) 219-5142','Connor','fermentum.arcu.Vestibulum@turpisnon.edu',NULL,'CBAC6F35-7EE2-7388-CD51-ECB76B1C4E9B',0,'9D8CE0AA-97C8-FB9C-536A-45346BF01A06',NULL,NULL),
(89,'Clark Maldonado','(353) 310-9303','Alec','nulla.magna.malesuada@sociisnatoquepenatibus.co.uk',NULL,'A364792D-B192-23B7-4709-4AFA77000F30',0,'B8F10D49-F562-091B-D6D1-727E4B3214EA',NULL,NULL),
(90,'Dennis Forbes','(601) 942-9683','Kelsie','feugiat.non@pulvinararcu.net',NULL,'1B6FA979-38A4-C4BF-D287-D26749CEFCCF',0,'F7D931E8-B618-04E3-49A4-C1F1EBCB6662',NULL,NULL),
(91,'Brenden Mcdowell','(412) 339-1313','September','tincidunt.tempus@nec.ca',NULL,'02E29CD2-3577-2A4E-9307-F1ABA0F1CADF',0,'CCBC61F4-B214-9F0D-4D49-2F52824C5000',NULL,NULL),
(92,'Blaze Mccoy','(733) 989-7646','Hyatt','sem.egestas.blandit@tacitisociosqu.net',NULL,'78B203BA-3740-8F04-CBC4-186D0D469EF4',0,'2A639035-FED1-3ECD-88E6-A663B01B3538',NULL,NULL),
(93,'Raja Nichols','(902) 269-5326','Yoshio','turpis.egestas@rutrum.edu',NULL,'B32EF8FA-13C3-3E6C-F65B-16E934CBF92C',0,'2341BEEA-319C-0A7C-CF2D-64C4DF06030C',NULL,NULL),
(94,'Knox Elliott','(161) 158-3820','Cade','egestas@massaVestibulumaccumsan.com',NULL,'ADE2105B-13F1-944A-4A0A-10D3237711B3',0,'8248D15E-B1A6-A75D-46DE-2FA349472B01',NULL,NULL),
(95,'Sebastian Hopper','(204) 119-4570','Patrick','enim.condimentum.eget@tristiquesenectuset.ca',NULL,'ADC4AB76-EFC6-122F-1522-8E48953017AD',0,'DE3073CE-2CED-F48C-01FF-4E62CC2CD8F4',NULL,NULL),
(126,'Garrett Hartman','(987) 474-2272','Candice','interdum.enim.non@parturientmontes.ca',NULL,'1D007432-2BA2-80BA-C462-40F0F3E60C6B',0,'359874E0-DEA2-EF8D-47F3-661A001F5889',NULL,NULL),
(127,'Kamal Carlson','(935) 415-4408','Brenda','eu.odio.Phasellus@nonarcu.net',NULL,'E7AD818B-CFB9-31A6-7291-9DCFBFD05BB0',0,'F32E0DCD-B980-4FC4-19E4-AF674D4EC92A',NULL,NULL),
(128,'Kenneth Gilmore','(125) 425-4052','Abraham','nulla.Donec@fermentum.org',NULL,'62BC7F37-B01B-0E61-C6C5-55AC1316DD1E',0,'A6AB5FB9-6ADB-E90C-4E5C-EB681C6222D2',NULL,NULL),
(129,'Alexander Ortiz','(438) 454-2289','Louis','cursus.Integer.mollis@elementum.com',NULL,'2E155C3C-2166-1825-70DD-70263B285509',0,'46D904EA-DF39-03DD-2BA6-4230BFEED96B',NULL,NULL),
(130,'Kyle Gentry','(437) 544-5744','Roth','In@habitantmorbitristique.org',NULL,'8621BAE1-ECB4-C0B9-357A-E6C034F14270',0,'B9D55CC5-E0E1-2875-7EFA-533263A1415C',NULL,NULL),
(131,'Wyatt Dunn','(933) 815-5016','Jack','purus.ac.tellus@telluslorem.ca',NULL,'521A2BA8-4143-3FFD-2240-DC3AAD2CCD81',0,'50E43C32-59B1-50E8-93D2-15F55F8C137B',NULL,NULL),
(132,'Nasim Travis','(173) 200-2258','Nehru','egestas.Aliquam.fringilla@sagittislobortis.edu',NULL,'AB83B363-B0DB-6FD8-7C2E-D018D018F176',0,'7D5AD1CC-0509-3247-1CDB-F5DACDC5B1AC',NULL,NULL),
(133,'Jakeem Knox','(831) 318-0727','Eve','Ut.semper@hendreritconsectetuercursus.com',NULL,'DEB49C05-5342-D193-2F3C-3CEC5B5C6B4E',0,'37974A17-EBF8-BA0F-C13E-F2AA76467495',NULL,NULL),
(134,'Dustin Gillespie','(416) 173-4504','Yasir','ultrices.sit.amet@estNunclaoreet.ca',NULL,'74152649-067D-00AB-C4CE-E571EC65493B',0,'F1206FFD-32A7-2160-D34B-4F4A579EF914',NULL,NULL),
(135,'Baker Eaton4','(985) 603-6762','Lacota','ut.nulla@erat.com',NULL,'$2y$10$W5H/jA4upo5WK8PqniwWF.8BlSFHPBfg7a639hKrcq93wzHsxmtAq',0,'B914D971-D97F-EDB9-D16C-912A945C2A7D',NULL,'2018-11-07 09:44:39'),
(146,'Eaton Vazquez','(222) 795-0570','Sophia','ante@luctus.org',NULL,'B61FE444-F60E-4D58-1473-0F76DE67D9AA',0,'8AFD5631-BC2F-9AC5-79C0-95245477E4DA',NULL,NULL),
(147,'Gregory Carrillo','(398) 954-5605','Zahir','nunc.est.mollis@Quisquenonummy.org',NULL,'D5705876-75DB-5ED0-87E1-7245E9194941',0,'39C24BF0-C94F-ECBA-C3B8-CE420C3C4A0D',NULL,NULL),
(148,'Raymond Glover','(704) 177-4706','Keelie','tempor.bibendum@Maurisquis.edu',NULL,'EBC2AFBF-9D97-5B5E-C8BA-533A2E4A8E93',0,'6C90BC35-555D-5591-FFB3-A99131E5894D',NULL,NULL),
(149,'Caleb Reilly','(948) 387-2750','Lucius','vel@lacusvestibulum.org',NULL,'5DDD9ABC-C89F-656A-9738-21246D5A4B04',0,'65B19C2E-BD51-319F-9F14-F4CEB1882E36',NULL,NULL),
(150,'Octavius Webster','(113) 883-3623','Brent','et.magnis@nonbibendum.net',NULL,'A5EE2E17-59A3-D083-611D-22F8CC2BE1CF',0,'DB0875BA-8366-9A02-BD5D-D5EDDF6DE737',NULL,NULL),
(151,'Chester Kinney','(439) 414-5409','Hilary','magnis@euismodestarcu.net',NULL,'11CE14BD-6FF0-92C6-4B65-38EE75FE6057',0,'BDB58ED6-F807-7D23-CF05-DD60AA85E1E5',NULL,NULL),
(152,'Elmo Yates','(468) 189-4245','Adele','dictum.mi@dictum.com',NULL,'0B0CC87F-B33C-0013-75EA-45351A364429',0,'DAA61B51-9137-208B-32AB-8FFFB175D888',NULL,NULL),
(153,'Elijah Berry','(249) 212-7169','Lacy','penatibus.et@Etiamgravida.edu',NULL,'7E436B98-5591-91C1-2A82-1B5467312C4F',0,'CB3D60DF-996A-59D5-48C8-172B39224526',NULL,NULL),
(154,'Gannon Barber','(905) 538-0123','Lawrence','Aliquam@est.ca',NULL,'1CC33E38-2653-D94E-A57A-D232230045BA',0,'1D378F32-F6E2-91E7-0AB2-0E84A668D040',NULL,NULL),
(155,'Ignatius Sargent','(140) 186-9884','Emerald','tempor.lorem@nisimagna.co.uk',NULL,'0D27C1A9-AE48-F3A7-9C7A-66885F396FC5',0,'D627EECE-0A24-5BF7-C24F-E53AFDCE8A12',NULL,NULL),
(176,'Abraham Mcneil','(775) 743-3886','Allen','neque.sed@vehicula.ca',NULL,'61476809-442B-5729-6DE6-F4AF4A7EC220',1,'A07C001D-9632-433F-C6E0-CF3D59DA03C9',NULL,'2018-11-06 11:23:11'),
(177,'Alfonso Burnett','(859) 578-5456','Raven','aliquam@justo.co.uk',NULL,'9B1D98B3-EC6D-5A1F-E7AE-F2B665BC2E84',1,'0FC6EE2E-160D-2D11-7041-8900237312CC',NULL,'2018-11-08 14:17:08'),
(178,'Forrest Wiggins','(896) 214-3701','Macaulay','lorem@Crasconvallisconvallis.edu',NULL,'41EFBEED-7EBC-AC78-0D59-31C8E889DC91',0,'2E18B6C7-9B8A-3FD5-9614-F921DED8167F',NULL,NULL),
(179,'Guy Gardner','(840) 776-7376','Ginger','Sed.eu.nibh@et.co.uk',NULL,'35324742-4DF1-D0F6-F574-ED9064F8D85B',0,'7DC15B8A-2CDC-C839-ECD4-AF358C2FAC44',NULL,NULL),
(180,'Dale Warner','(434) 948-3010','Hayfa','Donec.tincidunt@ipsum.org',NULL,'30EA0604-26DC-10FE-CC68-E27E549B8106',0,'5BC23024-5058-EE62-0DBD-69DCC8E74603',NULL,NULL),
(181,'Rajah Preston','(142) 790-7218','Bruno','pharetra.nibh@laoreetipsum.co.uk',NULL,'2F4E0ADB-F9C1-98F5-E2F1-E8A1318F83BB',0,'B32A0C87-779C-621C-A078-99EA946006AC',NULL,NULL),
(182,'Blake Pena','(891) 461-3168','Kelly','luctus@etipsumcursus.co.uk',NULL,'E8014BE0-2648-9F03-E2FA-24BF1E7C76F5',0,'0C889F6A-308E-260E-126D-6B65C99BD4DC',NULL,NULL),
(183,'Emery Mccullough','(488) 466-7575','Aimee','at.sem@senectuset.com',NULL,'34B719E2-D7E9-1428-DAFD-C47E0C22BD43',0,'CC6EF49E-B545-D7E3-EB0F-7B70076E68B4',NULL,NULL),
(184,'Castor Odom','(826) 478-4824','Sade','non@In.com',NULL,'167F60E2-25AE-C432-770C-07D6DBDA2ECB',0,'FA58DA2E-1172-FB4F-B20F-8C05AB32C77D',NULL,NULL),
(185,'Lucas Dodson','(713) 850-2431','Aretha','quis@ornareFuscemollis.ca',NULL,'6AD3B7EA-D194-99C4-9304-87688F39AF53',0,'B202D5E1-5950-02CB-C0BB-F2EF1DAD8BB0',NULL,NULL),
(196,'Christian Battle','(711) 794-2323','Cameran','Proin.vel@ornare.co.uk',NULL,'27F13CD0-FB3F-6C66-F7A7-1617C5DDAA91',0,'74E9B05D-51EC-95AE-95E0-FECB46C10F49',NULL,NULL),
(197,'Francis Petty','(178) 735-5038','Leila','mollis.nec@Aliquamvulputate.net',NULL,'4C3544E9-2705-826F-F97F-3FA8648F62E9',0,'46D56A71-39BC-D9E7-29AC-E0A841B74767',NULL,NULL),
(198,'Ryan Mcclure','(305) 405-6180','Karina','Sed.pharetra.felis@ettristique.com',NULL,'DDCC4BA1-BBA3-6FD7-E432-1481CCBB9969',0,'9BD38AFF-8D6A-0581-0C5E-B2BD35521BDB',NULL,NULL),
(199,'Lionel Jacobs','(708) 503-2742','Morgan','Nam.ac.nulla@rutrum.ca',NULL,'C5FDD6B5-D147-30FE-FC19-2C7578462D9B',0,'1DB60AC2-BE0C-9CD1-E86B-FE1C3F8EB537',NULL,NULL),
(200,'Raphael Marks','(469) 477-4490','Simone','Donec.dignissim.magna@non.com',NULL,'05CFB8A6-F93A-3400-44A6-A222631B5129',0,'042AEDFF-A7FB-AA4B-D4D6-B4E817B556AF',NULL,NULL),
(201,'Hayes Randolph','(116) 457-9122','Knox','vitae.aliquet@acmattis.org',NULL,'F6DE9BE2-A4D7-7A38-C815-D229FD5945A5',0,'DB494512-24C5-90F4-2DAF-95F2358045ED',NULL,NULL),
(202,'Tanner Jenkins','(798) 524-3167','Shelby','nisi.Mauris@ametorci.co.uk',NULL,'5AD924C2-3F4C-8C16-B33C-EF0DD888E55A',0,'DE7DD2D2-0DC8-405D-64CF-22EFBC202B20',NULL,NULL),
(203,'Gage Cotton','(295) 764-5057','Caldwell','placerat.eget.venenatis@nisiAenean.co.uk',NULL,'53BD023D-6A53-103B-72DB-FDD362AF669C',0,'CF25E9C2-536E-B2AC-8C46-7D7AA1B80D35',NULL,NULL),
(204,'Zachary Shepard','(841) 691-6643','Peter','dignissim.lacus.Aliquam@Fusce.com',NULL,'B1AB4E7B-CCEE-ED22-1849-051643CAC0CB',0,'8AE5C4CE-7A70-498B-8E4E-1B49B7B5CCDB',NULL,NULL),
(205,'Julian Lucas','(972) 458-2669','Oleg','Duis@risusQuisque.com',NULL,'9DBC0DEE-4430-8969-3D6B-C3EF1FFBE7D8',0,'361C32B1-78EC-BCBC-5B3D-17801B30B934',NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
