function formatCurrency(x) {
	x = parseFloat(x).toFixed(2);
    x = x.toString();
    var pattern = /(-?\d+)(\d{3})/;
    while (pattern.test(x))
        x = x.replace(pattern, "$1,$2");
    return x;
}

function formatNumber(value, decimal = 0) {
	try {
		num = parseFloat(value);

		if (isNaN(num))
			num = 0;

		num = num.toFixed(decimal);
		num = num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
		return num;
	} catch (err) {
		var x = 0;
		return x.toFixed(decimal);
	}
}