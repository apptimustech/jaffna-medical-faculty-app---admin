function createDropDownStr(data){
    var htmlStr = "";
    $.each(data, function(i, val){
        htmlStr += "<option value='"+val.id+"'>"+val.name+"</option>";
    });
    return htmlStr;
 }

 function showSnackbar(msg) {
    var x = document.getElementById("snackbar");
    x.innerHTML = msg;
    x.className = "show";
    
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 4000);
  }

  function getURLParameter(parameter) {
    var pValue = "";
    try
    {
        var vars = window.location.search.split("?")[1].split("&");
        var query_string = {};
        for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        var key = decodeURIComponent(pair[0]);
        var value = decodeURIComponent(pair[1]);

        if (typeof query_string[key] === "undefined") {
            query_string[key] = decodeURIComponent(value);
            // If second entry with this name
        } else if (typeof query_string[key] === "string") {
            var arr = [query_string[key], decodeURIComponent(value)];
            query_string[key] = arr;
            // If third or later entry with this name
        } else {
            query_string[key].push(decodeURIComponent(value));
        }
        }

        //console.log(query_string);
        pValue = query_string[parameter];
        pValue = pValue ? pValue : "";
    }catch(err)
    {
        //console.error(err);
    }
    return pValue;
  }