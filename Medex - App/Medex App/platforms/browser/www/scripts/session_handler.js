function setSession(token, user, department, settings)
{
    localStorage.setItem("sessionToken", token);
    localStorage.setItem("sessionUser", JSON.stringify(user));
    localStorage.setItem("sessionDept", JSON.stringify(department));
    localStorage.setItem("sessionSetting", JSON.stringify(settings));
}

function getSessionToken()
{
    return localStorage.getItem("sessionToken");
}

function getSessionUser()
{
    return JSON.parse(localStorage.getItem("sessionUser") || "{}");
}

function getSessionDepartment()
{
    return JSON.parse(localStorage.getItem("sessionDept") || "{}");
}

function getSessionSettings()
{
    return JSON.parse(localStorage.getItem("sessionSetting") || "{}");
}

//Register status
function setSessionRegisterStatus(status, code)
{
    localStorage.setItem("registerStatus", status);
    localStorage.setItem("registerCode", code);
}

function getSessionRegisterStatus()
{
    return localStorage.getItem("registerStatus");
}

function getSessionRegisterCode()
{
    return localStorage.getItem("registerCode");
}

function logout()
{
	localStorage.removeItem("sessionToken");
    localStorage.removeItem("sessionUser");
    localStorage.removeItem("sessionDept");
    localStorage.removeItem("sessionSetting");
	localStorage.removeItem("registerStatus");
    localStorage.removeItem("registerCode");	
    window.location.assign("index.html");
}