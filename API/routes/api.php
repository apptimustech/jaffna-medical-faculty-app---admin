<?php
use Illuminate\Http\Request;
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,POST,DELETE');

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::post('login', 'Auth\LoginController@login');
//==============================================================================
// ADMIN OPERATIONS - Admin Middleware
//==============================================================================
Route::group(['middleware' => 'adminMiddleware'], function(){
    //****************************************************************
    //Users Operations
    //****************************************************************
    Route::get('/users', 'UserController@getAll');
    Route::get('/users/{id}', 'UserController@getOne');
    Route::post('/users', 'UserController@add');
    Route::post('/users/{id}', 'UserController@update');
    Route::delete('/users/{id}', 'UserController@delete');
    Route::get('/me', 'UserController@me'); 

    //Tags 
    Route::post('/tags', 'TagController@add');
    Route::post('/tags/{id}', 'TagController@update');
    Route::delete('/tags/{id}', 'TagController@delete');

    //Attachments 
    Route::post('/attachments', 'AttachmentController@add');
    Route::post('/attachments/{id}', 'AttachmentController@update');
    Route::delete('/attachments/{id}', 'AttachmentController@delete');

    //Category
    Route::post('/categories', 'CategoryController@store');
    Route::post('/categories/{id}', 'CategoryController@update');
    Route::delete('/categories/{id}', 'CategoryController@destroy');

    //Content
    Route::post('/contents', 'ContentController@store');
    Route::post('/contents/{id}', 'ContentController@update');
    Route::delete('/contents/{id}', 'ContentController@destroy');


    //Content_has-tags    
    Route::post('/content-tags', 'ContentHasTagController@store');
    Route::delete('/content-tags/{id}', 'ContentHasTagController@destroy');

    //User-impression
    Route::post('/user-impressions', 'UserImpressionController@store');

    //Report 
    Route::get('/report/view', 'ReportController@index');
    Route::get('/report/view/{id}', 'ReportController@show');
});




Route::group(['middleware' => 'publicMiddleware'], function(){
  Route::post('/admin-login', 'UserController@login');
  Route::post('/register', 'API\AuthController@register');

  Route::get('/tags', 'TagController@getAll');
  Route::get('/tags/{id}', 'TagController@getOne');

  Route::get('/categories', 'CategoryController@index');
  Route::get('/categories/{id}', 'CategoryController@show');

  Route::get('/contents', 'ContentController@index');
  Route::get('/contents/{cid}/tags', 'ContentController@showTags');
  Route::get('/search', 'ContentController@searchContent');
  Route::get('/contents/{id}', 'ContentController@show');

  Route::get('/download/{id}', 'AttachmentController@getDownload');
  Route::get('/attachments', 'AttachmentController@getAll');
  Route::get('/attachments/{id}', 'AttachmentController@getOne');
});

Route::group(['middleware' => 'auth:api'], function(){
Route::post('get-details', 'API\AuthController@getDetails');

});