<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use DB;

class AdminAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = null;
        try
        {
            if (!$request->has("token"))
            {
                $response["msg"]        = "Unauthorized Access - Token not found";
                $response["status"]     = "Failed";
                $response["is_success"] = false;
                return Response($response, 401);
            }
            else
            {
                $token = $request->token;
                $user  = DB::table('users')->where('token', $token)->first();

                if ($user != null)
                {
                    return $next($request);
                }
                else
                {
                    $response["msg"]        = "Unauthorized Access - Invalid Token";
                    $response["status"]     = "Failed";
                    $response["is_success"] = false;
                    return Response($response, 401);
                }
            }
        }
        catch (Exception $e)
        {
            $response["msg"]        = $e->getMessage();
            $response["status"]     = "Failed";
            $response["is_success"] = false;
        }

        $response["msg"]        = "Operation failed. Please try again.";
        $response["status"]     = "Failed";
        $response["is_success"] = false;
        return Response($response, 401);
    }
}
