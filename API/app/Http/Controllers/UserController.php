<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Validator;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return User::where('is_deleted', 0)->orderBy('name', 'asc')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getOne($id)
    {
        $user = User::find($id);
        if ($user->is_deleted == 1) {
            $response["msg"] = "Required user is not found";
            $response["status"] = "Failed";
            $response["is_success"] = false;
        } else {
            $response["msg"] = "Required user is  found";
            $response["status"] = "Success";
            $response["is_success"] = true;
            $response["data"] = $user;
        }
        return $response;

    }

    /** ADD NEW RESOURCE **/
    public function add(Request $request)
    {
        $response = array('status' => 'Failed','msg' => '', 'is_success'=>false);

        try {
            $rules =
            [
                    'name' => 'required|max:45',
                    'email' => 'unique:users|max:45',
                    'contact' => 'required',
                    //'contact' => 'required|regex:/^\+[0-9]{11}$/',
                    'user_name' => 'required|unique:users|max:45',
                    'password' => 'required|max:200|min:6',
                    //'is_deleted' => 'required',

            ];

            $customMessages =
            [
                'name.required' => 'Name cannot be empty.',
                'email.unique' => 'Email ID already found',
                'user_name.required' => 'Username cannot be empty',
                'user_name.unique' => 'Username already found',
                'password.required' => 'Password cannot be empty.',
                //'is_deleted.required' => 'is_deleted cannot be empty.',
            ];

            $validator = Validator::make($request->all(), $rules, $customMessages);
            if ($validator->fails()) {
                $response["msg"] = $validator->messages()->first();
                $response["status"] = "Failed";
                $response["is_success"] = false;
            }
            else
            {
                $user = new User();
                $user->name = $request->name;
                $user->email = $request->email;
                $user->contact = $request->contact;
                $user->user_name = $request->user_name;
                $user->password = Hash::make($request->password);
                $user->is_deleted = false;

                $newToken = str_random(30);
                $user->token = $newToken;

                if($user->save())
                {
                    $response["data"] = $user;
                    $response["msg"] = "User added successfully";
                    $response["status"] = "Success";
                    $response["is_success"] = true;
                }
                else
                {
                    $response["msg"] = "Operation failed. Please try again.";
                    $response["status"] = "Failed";
                    $response["is_success"] = false;
                }
            }
        }
        catch (Exception $e) {
            $response["msg"] =  $e->getMessage();
            $response["status"] = "Failed";
            $response["is_success"] = false;
        }
        return $response;
    }

    /** UPDATE A RESOURCE **/
    public function update(Request $request, $id)
    {
        $response = array('status' => 'Failed', 'msg' => '', 'is_success' => false);

        try {
            $rules =
                [
                'name' => 'required|max:225',
                'contact' => 'required',
                'email' => 'max:45|unique:users,id,{$id}',
                'user_name' => 'required|unique:users,id,{$id}|max:45',
                //'user_name' => 'required||unique:users,id,{$id}|regex:/^[a-zA-Z]+$/u|max:45',
                //'password' => 'required|max:200|min:6',

            ];

            $customMessages =
                [
                'name.required' => 'Name cannot be empty.',
                'email.unique' => 'Email ID already found',
                'user_name.required' => 'User name cannot be empty',
                'user_name.unique' => 'User name already found',
                //'password.required' => 'Password cannot be empty.',
            ];
            $validator = Validator::make($request->all(), $rules, $customMessages);
            if ($validator->fails()) {
                $response["msg"] = $validator->messages()->first();
                $response["status"] = "Failed";
                $response["is_success"] = false;
            } else {
                $user = User::find($id);

                if ($user != null) {

                    $user = User::find($id);
                    $user->name = $request->name;
                    $user->email = $request->email;
                    $user->contact = $request->contact;
                    $user->user_name = $request->user_name;
                    $user->password = Hash::make($request->password);
                    //$user->is_deleted = $request->is_deleted;

                    if ($user->save()) {
                        $response["msg"] = "User updated successfully";
                        $response["status"] = "Success";
                        $response["is_success"] = true;
                    } else {
                        $response["msg"] = "Operation failed. Please try again.";
                        $response["status"] = "Failed";
                        $response["is_success"] = false;
                    }
                } else {
                    $response["msg"] = "Invalid ID";
                    $response["status"] = "Failed";
                    $response["is_success"] = false;
                }
            }
        } catch (Exception $e) {
            $response["msg"] = $e->getMessage();
            $response["status"] = "Failed";
            $response["is_success"] = false;
        }
        return $response;
    }

    public function delete($id)
    {
        $user = User::find($id);

        if ($user != null) {

            $user = User::find($id);
            $user->is_deleted = 1;

            if ($user->save()) {
                $response["msg"] = "User deleted successfully";
                $response["status"] = "Success";
                $response["is_success"] = true;
            } else {
                $response["msg"] = "Operation failed. Please try again.";
                $response["status"] = "Failed";
                $response["is_success"] = false;
            }
        } else {
            $response["msg"] = "Invalid ID";
            $response["status"] = "Failed";
            $response["is_success"] = false;
        }

        return $response;
    }

    /** GET DETAILES OF LOGGED IN USERS - MY PROFILE **/
    public function me(Request $request)
    {
        $token = $request->token;
        $user = User::where('token', '=', $token)->first();
        return $user;
    }

    public function login(Request $request)
    {
        $uname = $request->user_name;
        $pword = $request->password;

        $rules =
            [
            'user_name' => 'required',
            'password' => 'required',
        ];

        $customMessages =
            [
            'user_name.required' => 'Username cannot be empty.',
            'password.required' => 'Password cannot be empty',
        ];

        $validator = Validator::make($request->all(), $rules, $customMessages);
        if ($validator->fails()) {
            $response["msg"] = $validator->messages()->first();
            $response["status"] = "Failed";
            $response["is_success"] = false;
            return Response($response, 200);
        } else {
            $user = User::
                where('user_name', '=', $uname)
                ->where('password', '=', $pword)->first();

            //print_r($result);

            if ($user) {
                $user->token = str_random(30);
                $user->save();

                $response["msg"] = "Successfully logged in";
                $response["status"] = "Success";
                $response["is_success"] = true;
                $response["user"] = $user;
                $response["token"] = $user->token;
                return Response($response, 200);
            } else {
                $response["msg"] = "Wrong username or password";
                $response["status"] = "Failed";
                $response["is_success"] = false;
                return Response($response, 200);
            }
        }
    }
}
