<?php
namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Validator;
use Storage;

class TagController extends Controller
{

    public function getAll()
    {
        return Tag::all();
    }

    public function getOne($id)
    {
        $tag = Tag::find($id);
        return $tag;
    }

    public function add(Request $request)
    {
        $response = array(
            'status' => 'Failed',
            'msg' => '',
            'is_success' => false
        );

        try
        {
            $rules = ['name' => 'required|max:50', ];

            $customMessages = ['name.required' => 'Name cannot be empty.', ];

            $validator = Validator::make($request->all() , $rules, $customMessages);
            if ($validator->fails())
            {
                $response["msg"] = $validator->messages()->first();
                $response["status"] = "Failed";
                $response["is_success"] = false;
            }
            else
            {
                $tag = new Tag();
                $tag->name = $request->name;

                if ($tag->save())
                {
                    $response["data"] = $tag;
                    $response["msg"] = "Tag added successfully";
                    $response["status"] = "Success";
                    $response["is_success"] = true;
                }
                else
                {
                    $response["msg"] = "Operation failed. Please try again.";
                    $response["status"] = "Failed";
                    $response["is_success"] = false;
                }
            }
        }
        catch(Exception $e)
        {
            $response["msg"] = $e->getMessage();
            $response["status"] = "Failed";
            $response["is_success"] = false;
        }
        return $response;
    }

    //UPDATE A TAG
    public function update(Request $request, $id)
    {
        $response = array('status' => 'Failed', 'msg' => '', 'is_success' => false);

        try {
            $rules = [
                'name' => 'required|max:50|unique:categories,id, {{$id}}',
            ];

            $customMessages = [
                'name.required' => 'Name cannot be empty.',
                'name.unique' => 'This name has already been taken. Please try with another.',
            ];

            $validator = Validator::make($request->all(), $rules, $customMessages);
            if ($validator->fails()) {
                $response["msg"] = $validator->messages()->first();
                $response["status"] = "Failed";
                $response["is_success"] = false;
            } else {
                $tag = Tag::find($id);

                if ($tag != null) {
                    $tag->name = $request->name;

                    if ($tag->save()) {
                        $response["msg"] = "Category updated successfully";
                        $response["status"] = "Success";
                        $response["is_success"] = true;
                    } else {
                        $response["msg"] = "Operation failed. Please try again.";
                        $response["status"] = "Failed";
                        $response["is_success"] = false;
                    }
                } else {
                    $response["msg"] = "Invalid ID";
                    $response["status"] = "Failed";
                    $response["is_success"] = false;
                }
            }
        } catch (Exception $e) {
            $response["msg"] = $e->getMessage();
            $response["status"] = "Failed";
            $response["is_success"] = false;
        }
        return $response;
    }

    //DELETE A TAG
    public function delete($id)
    {
        $response = array('status' => 'Failed', 'msg' => '', 'is_success' => false);

        try {
            if ($id == 0 || $id == "" || $id == null) {
                $response["msg"] = "Invalid ID";
                $response["status"] = "Failed";
                $response["is_success"] = false;
            } else {
                $tag = Tag::find($id);
                if ($tag->delete()) {
                    $response["msg"] = "Tag deleted successfully";
                    $response["status"] = "Success";
                    $response["is_success"] = true;
                } else {
                    $response["msg"] = "Operation failed. Please try again.";
                    $response["status"] = "Failed";
                    $response["is_success"] = false;
                }
            }
        } catch (Exception $e) {
            $response["msg"] = $e->getMessage();
            $response["status"] = "Failed";
            $response["is_success"] = false;
        }
        return $response;
    }
}