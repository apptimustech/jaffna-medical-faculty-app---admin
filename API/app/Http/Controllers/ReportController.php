<?php
namespace App\Http\Controllers;

use App\UserImpression;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Validator;
use Storage;

class ReportController extends Controller
{
    //GET THE WHOLE CONTENT DATA REPORT
    public function index()
    {   
        $response = array(
            'status' => 'Failed',
            'msg' => '',
            'is_success' => false,
        );
        try{
        $userImpression = new UserImpression;

        if(Input::get('start_date') !=null && Input::get('end_date') != null){

            $filtered = UserImpression::whereBetween('created_at', [Input::get('start_date'), Input::get('end_date')])->get();
            
            $response = array(
                'status' => 'Success',
                'msg' => 'This is the data(s) between the dates you entered',
                'is_success' => True,
            );

            $response['data'] = ['view'=>$filtered];
        
        }else{
            $response = array(
            'status' => 'Success',
            'msg' => 'The whole data',
            'is_success' => True,
            );

        $totalCount = $userImpression->sum('value');
        $response['data'] = ['view'=>$userImpression->get(),'Total viewed'=>$totalCount];
        
         }
        }catch(Exception $e){

            $response["msg"] = $e->getMessage();
            $response["status"] = "Failed";
            $response["is_success"] = false;

        }
    return $response;
}
    //GET ONE CONTENT REPORT
    public function show($id)
    {
        $response = array(
            'status' => 'Failed',
            'msg' => '',
            'is_success' => false,
        );
    try{
        $userImpression = new UserImpression;
        $count = UserImpression::where('content_id','=',$id)->sum('value');
        $contentData = UserImpression::where('content_id','=',$id)->get();
        $response = array(
            'status' => 'Success',
            'msg' => 'This is the particular content report',
            'is_success' => true,
        );
        $response['data'] = ['view'=>$contentData,'viewed'=>$count];
         
    }catch(Exception $e)
        {
            $response["msg"] = $e->getMessage();
            $response["status"] = "Failed";
            $response["is_success"] = false;
        }   
        return $response;
    }
}