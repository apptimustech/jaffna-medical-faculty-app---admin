<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Category::orderBy('name', 'asc')->get();
    }

    //GET ALL CATEGORY
    public function create()
    {
        //
    }

    //ADD NEW CATEGORY
    public function store(Request $request)
    {
        $response = array('status' => 'Failed', 'msg' => '', 'is_success' => false);

        try {
            $rules = [
                'name' => 'required|max:50|unique:categories',
                //'parent_id' => 'required',
            ];

            $customMessages = [
                'name.required' => 'Category Name cannot be empty.',
                //'parent_id.required' => 'Parent_id cannot be empty.',
                'name.unique' => 'This name has already been taken. Please try with another.',
            ];

            $validator = Validator::make($request->all(), $rules, $customMessages);
            if ($validator->fails()) {
                $response["msg"] = $validator->messages()->first();
                $response["status"] = "Failed";
                $response["is_success"] = false;
            } else {
                $category = new Category();
                $category->name = $request->name;
                $category->description = $request->description;
                $category->description = "DEFAULT-CONTENT-BG.jpg";
                
                 //Values(files) upload
                 if ($request->hasFile('dp')) {
                    $image = $request->file('dp');
                    $name = "CATEGORY_DP_".rand(1000000,10000000).time().'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/download');
                    $image->move($destinationPath, $name); 
                    $filepath =  $name;  
                    $category->dp = $filepath;        
                }
                

                if ($request->parent_id != "" && $request->parent_id != 0) {
                    $category->parent_id = $request->parent_id;
                } else {
                    $category->parent_id = null;
                }

                if ($category->save()) {
                    $response["data"] = $category;
                    $response["msg"] = "Category added successfully";
                    $response["status"] = "Success";
                    $response["is_success"] = true;
                } else {
                    $response["msg"] = "Operation failed. Please try again.";
                    $response["status"] = "Failed";
                    $response["is_success"] = false;
                }
            }
        } catch (Exception $e) {
            $response["msg"] = $e->getMessage();
            $response["status"] = "Failed";
            $response["is_success"] = false;
        }
        return $response;
    }

    //GET ONE CATEGORY
    public function show($id)
    {
        $category = Category::find($id);
        return $category;
    }

    //
    public function edit(Category $category)
    {
        //
    }

    //UPDATE A CATEGORY
    public function update(Request $request, $id)
    {
        $response = array('status' => 'Failed', 'msg' => '', 'is_success' => false);

        try {
            $rules = [
                'name' => 'required|max:50|unique:categories,id, {{$id}}',
                //'parent_id' => 'required',
            ];

            $customMessages = [
                'name.required' => 'Name cannot be empty.',
                //'parent_id.required' => 'Parent_id cannot be empty.',
                'name.unique' => 'This name has already been taken. Please try with another.',
            ];

            $validator = Validator::make($request->all(), $rules, $customMessages);
            if ($validator->fails()) {
                $response["msg"] = $validator->messages()->first();
                $response["status"] = "Failed";
                $response["is_success"] = false;
            } else {
                $category = Category::find($id);

                if ($category != null) {
                    $category->name = $request->name;
                    $category->description = $request->description;

                    //Values(files) upload
                    if ($request->hasFile('dp')) {
                        $image = $request->file('dp');
                        $name = "CATEGORY_DP_".rand(1000000,10000000).time().'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('/download');
                        $image->move($destinationPath, $name); 
                        $filepath =  $name;  
                        $category->dp = $filepath;        
                    }

                    if ($request->parent_id != "" && $request->parent_id != 0) {
                        $category->parent_id = $request->parent_id;
                    } else {
                        $category->parent_id = null;
                    }

                    if ($category->save()) {
                        $response["msg"] = "Category updated successfully";
                        $response["status"] = "Success";
                        $response["is_success"] = true;
                    } else {
                        $response["msg"] = "Operation failed. Please try again.";
                        $response["status"] = "Failed";
                        $response["is_success"] = false;
                    }
                } else {
                    $response["msg"] = "Invalid ID";
                    $response["status"] = "Failed";
                    $response["is_success"] = false;
                }

            }
        } catch (Exception $e) {
            $response["msg"] = $e->getMessage();
            $response["status"] = "Failed";
            $response["is_success"] = false;
        }
        return $response;
    }

    //DELETE A CATEGORY
    public function destroy($id)
    {
        $response = array('status' => 'Failed', 'msg' => '', 'is_success' => false);

        try {
            if ($id == 0 || $id == "" || $id == null) {
                $response["msg"] = "Invalid ID";
                $response["status"] = "Failed";
                $response["is_success"] = false;
            } else {
                $category = Category::find($id);
                if ($category->delete()) {
                    $response["msg"] = "Category deleted successfully";
                    $response["status"] = "Success";
                    $response["is_success"] = true;
                } else {
                    $response["msg"] = "Operation failed. Please try again.";
                    $response["status"] = "Failed";
                    $response["is_success"] = false;
                }
            }
        } catch (Exception $e) {
            $response["msg"] = $e->getMessage();
            $response["status"] = "Failed";
            $response["is_success"] = false;
        }
        return $response;
    }
}
