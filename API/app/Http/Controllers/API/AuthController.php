<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

use Illuminate\Support\Facades\Auth;

use Validator;
class AuthController extends Controller
{
    public $successStatus = 200;

    public function login(){
        if(Auth::attempt(['user_name' => request('user_name'), 'password' => request('password'),'is_deleted'=>0])){
            {
            $user = Auth::user();
            //$success['remember_token'] = $user->createToken('MyApp')->accessToken;
            $response["msg"] = 'Successfully logged into the system';
            $response["status"] = "success";
            $response["is_success"] = true;
            $response["remember_token"] = $user->createToken('MyApp')->accessToken;

            return $response;
            }
            
        }else{
            $response["msg"] = 'User name and password are not matching';
              $response["status"] = "Failed";
              $response["is_success"] = false;
            return $response;
        }
    }


    public function register(Request $request)
    {
            $rules = 
            [
                    //'name' => 'required|max:45',
                    'name' => 'required|max:225',
                    'email' => 'unique:users|max:45|min:6',
                    'contact' => 'required|numeric|min:9',
                    'user_name' => 'required|unique:users|max:45',
                    'password' => 'required|max:200|min:6',
                    'confirm_password'=> 'required|same:password',


                   
            ];

            $customMessages =
            [
                'name.required' => 'Name cannot be empty.',
                'email.unique' => 'Email ID already found',
                'user_name.required' => 'User name cannot be empty',
                'user_name.unique' => 'User name already found',
                //'contact.regex:' => 'Contact must contain only numbers Eg:- 0775345635',
                'password.required' => 'Password cannot be empty.',
                'confirm_password.required' => 'Confirm password cannot be empty.',
            ];

            $validator = Validator::make($request->all(), $rules, $customMessages);
             if($validator->fails()){
                $response["msg"] = $validator->messages()->first();
                $response["status"] = "Failed";
                $response["is_success"] = false;
                return $response;
                }

                else{
                $input = $request->all();
                $input['password'] = bcrypt($input['password']);
                //$input['user_name'] = '$input['user_name'];
                $user = User::create($input);
                $success['remember_token'] = $user->createToken('MyApp')->accessToken;
                $success['name'] =$user->name;
                return response()->json(['success'=>$success], $this->successStatus);
                }
            }
            
                
            
                public function getDetails()
                {
                    $user = Auth::user();
                    return response()->json(['success' => $user], $this->successStatus);
                }
            }
            
