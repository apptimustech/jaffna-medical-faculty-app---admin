<?php

namespace App\Http\Controllers;

use App\UserImpression;
use Illuminate\Http\Request;
use Validator;
use Storage;

class UserImpressionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    //ADD NEW USER-IMPRESSION
    public function create()
    {
        
    }

    //ADD NEW USER-IMPRESSION
    public function store(Request $request)
    {
        $response = array('status' => 'Failed','msg' => '', 'is_success'=>false);

        try {
            $rules = [
                  'type' => 'required|max:20|unique:user_impressions',
                  'value' => 'required|max:50',
            ];
  
            $customMessages = [
                'type.required' => 'Type cannot be empty.',
                'type.unique' => 'This type has already been taken. Please try with another.',
            ];
  
            $validator = Validator::make($request->all(), $rules, $customMessages);
            if ($validator->fails()) {
                $response["msg"] = $validator->messages()->first();
                $response["status"] = "Failed";
                $response["is_success"] = false;
            }
            else
            {
                $userImpression = new UserImpression();
                $userImpression->type = $request->type;
                $userImpression->value = $request->value;
                $userImpression->date_time = $request->date_time;
                $userImpression->ip = $request->ip;
                $userImpression->content_id = $request->content_id;
  
                if($userImpression->save())
                {
                  $response["msg"] = "User-impression added successfully";
                  $response["status"] = "Success";
                  $response["is_success"] = true;
                }
                else
                {
                  $response["msg"] = "Operation failed. Please try again.";
                  $response["status"] = "Failed";
                  $response["is_success"] = false;
                }
            }
        }
        catch (Exception $e) {
            $response["msg"] =  $e->getMessage();
            $response["status"] = "Failed";
            $response["is_success"] = false;
        }
        return $response;
    }

    //
    public function show(UserImpression $userImpression)
    {
        //
    }

    //
    public function edit(UserImpression $userImpression)
    {
        //
    }

    //
    public function update(Request $request, UserImpression $userImpression)
    {
        //
    }

    //
    public function destroy(UserImpression $userImpression)
    {
        //
    }
}
