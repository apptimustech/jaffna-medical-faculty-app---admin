<?php

namespace App\Http\Controllers;

use App\ContentHasTag;

use Illuminate\Http\Request;
use Validator;
use Storage;

class ContentHasTagController extends Controller
{
    //GET ALL CONTENT
    public function index()
    {
       
    }

    //
    public function create()
    {
        //
    }

    //ADD NEW CONTENT
    public function store(Request $request)
    {
        $response = array('status' => 'Failed','msg' => '', 'is_success'=>false);

        try {
            $rules = [
                  'contents_id' => 'required',
                  'tags_id' => 'required',
            ];
  
            $customMessages = [
                'contents_id.required' => 'Content cannot be empty.',
                'tags_id.required' => 'Tag cannot be empty.',
            ];
  
            $validator = Validator::make($request->all(), $rules, $customMessages);
            if ($validator->fails()) {
                $response["msg"] = $validator->messages()->first();
                $response["status"] = "Failed";
                $response["is_success"] = false;
            }
            else
            {
                $contentHasTag = new ContentHasTag();
                $contentHasTag->contents_id = $request->contents_id;
                $contentHasTag->tags_id = $request->tags_id;
  
                if($contentHasTag->save())
                {
                  $response["msg"] = "Content tag added successfully";
                  $response["status"] = "Success";
                  $response["is_success"] = true;
                }
                else
                {
                  $response["msg"] = "Operation failed. Please try again.";
                  $response["status"] = "Failed";
                  $response["is_success"] = false;
                }
            }
        }
        catch (Exception $e) {
            $response["msg"] =  $e->getMessage();
            $response["status"] = "Failed";
            $response["is_success"] = false;
        }
        return $response;
    }

    //GET ONE CONTENT
    public function show($id)
    {
       
    }

    
    //UPDATE A CONTENT
    public function update(Request $request, $id)
    {
        
    }

    //DELETE A CONTENT
    public function destroy(Request $request, $id)
    {
        $response = array('status' => 'Failed','msg' => '', 'is_success'=>false);

      try {
          if ($id == 0 || $id == "" || $id == null) {
              $response["msg"] = "Invalid ID";
              $response["status"] = "Failed";
              $response["is_success"] = false;
          }
          else
          {
              $contentHasTag = ContentHasTag::where('tags_id', $id)->where('contents_id', $request->cid)->first();
              if($contentHasTag->delete())
              {
                $response["msg"] = "Content tag deleted successfully";
                $response["status"] = "Success";
                $response["is_success"] = true;
              }
              else
              {
                $response["msg"] = "Operation failed. Please try again.";
                $response["status"] = "Failed";
                $response["is_success"] = false;
              }
          }
      }
      catch (Exception $e) {
          $response["msg"] =  $e->getMessage();
          $response["status"] = "Failed";
          $response["is_success"] = false;
      }
      return $response;
    }
}
