<?php

namespace App\Http\Controllers;

use App\Content;
use App\Category;
use App\UserImpression;
use App\Tag;
use Illuminate\Http\Request;
use Validator;
use Storage;
use DB;
use Illuminate\Support\Facades\Input;
use App\ContentHasTag;

class ContentController extends Controller
{
    //GET ALL CONTENT
    public function index()
    {
        $contents = Content::with('tags','attachments')->get();
        $categories = Category::orderBy('name', 'asc')->get();
        $response['data'] = ['contents'=>$contents,'categories'=>$categories];
        return $response;
    }

    //
    public function create()
    {
        //
    }

    //ADD NEW CONTENT
    public function store(Request $request)
    {
        $response = array('status' => 'Failed','msg' => '', 'is_success'=>false);

      try {
          $rules = [
                'title' => 'required|max:50|unique:contents',
                'category_id' => 'required',
          ];

          $customMessages = [
              'title.required' => 'Content title cannot be empty.',
              'category_id.required' => 'category_id cannot be empty.',
              'title.unique' => 'This name has already been taken. Please try with another.',
          ];

          $validator = Validator::make($request->all(), $rules, $customMessages);
          if ($validator->fails()) {
              $response["msg"] = $validator->messages()->first();
              $response["status"] = "Failed";
              $response["is_success"] = false;
          }
          else
          {
              $content = new Content();
              $content->title = $request->title;
              $content->body = $request->body;
              $content->category_id = $request->category_id;

              if($content->save())
              {
                $response["id"] = $content->id;
                $response["msg"] = "content added successfully";
                $response["status"] = "Success";
                $response["is_success"] = true;
              }
              else
              {
                $response["msg"] = "Operation failed. Please try again.";
                $response["status"] = "Failed";
                $response["is_success"] = false;
              }
          }
      }
      catch (Exception $e) {
          $response["msg"] =  $e->getMessage();
          $response["status"] = "Failed";
          $response["is_success"] = false;
      }
      return $response;
    }

    //GET ONE CONTENT
    public function show($id)
    {
        $userImpression = new UserImpression;
        $userImpression->type = 'CONTENT';
        $userImpression->content_id = $id;
        $userImpression->type = 'view';
        $userImpression->ip = request()->ip();
        $userImpression->value = 1;
        $userImpression->save();

        $content = Content::with('tags','attachments')->find($id);
        $category = Category::find($content->category_id);

        $count = UserImpression::where('content_id','=',$id)->where('type','=','view')->sum('value');
        $response['data'] = ['content'=>$content,'views'=>$count, 'category' => $category,  'allTags' => Tag::all()];
        return $response;
    }
    
    public function showTags($cid)
    {
        $contentTags = Content::with('tags')->find($cid);
        return $contentTags;
    }
    
    //Search the contents
    public function searchContent(Content $content)
    {
        try
        {
        $data = Input::get('search');
        $searchcontent = Content::whereHas('tags', function($q) use($data)
                {
                $q->where('name', 'like', '%'.$data.'%');
                });

        $searchcontent = $searchcontent->orWhere('title', 'LIKE', '%'.$data.'%')->get();
        
                if(count($searchcontent) != 0)
                {
                    $response["msg"] = "Content found ";
                    $response["status"] = "Success";
                    $response["is_success"] = true;
                    $response["data"] = $searchcontent;
                  }
                  else
                  {
                    $response["msg"] = "Required content not found";
                    $response["status"] = "Failed";
                    $response["is_success"] = false;
                  }
          
        }
        catch (Exception $e) {
            $response["msg"] =  $e->getMessage();
            $response["status"] = "Failed";
            $response["is_success"] = false;
            
        }
        return $response;
    }

    //UPDATE A CONTENT
    public function update(Request $request, $id)
    {
        $response = array('status' => 'Failed','msg' => '', 'is_success'=>false);

      try {
        $rules = [
              'title' => 'required|max:50|unique:contents,id, {{$id}}',
              'category_id' => 'required',
        ];

        $customMessages = [
            'title.required' => 'Title cannot be empty.',
            'category_id.required' => 'category_id cannot be empty.',
            'title.unique' => 'This title has already been taken. Please try with another.',
        ];

          $validator = Validator::make($request->all(), $rules, $customMessages);
          if ($validator->fails()) {
              $response["msg"] = $validator->messages()->first();
              $response["status"] = "Failed";
              $response["is_success"] = false;
          }
          else
          {
              $content = Content::find($id);

              if($content != null)
              {
                $content->title = $request->title;
                $content->body = $request->body;
                $content->category_id = $request->category_id;

                if($content->save())
                {
                  $response["msg"] = "Content updated successfully";
                  $response["status"] = "Success";
                  $response["is_success"] = true;
                }
                else
                {
                  $response["msg"] = "Operation failed. Please try again.";
                  $response["status"] = "Failed";
                  $response["is_success"] = false;
                }
              }
              else
              {
                $response["msg"] = "Invalid ID";
                $response["status"] = "Failed";
                $response["is_success"] = false;
              }

          }
      }
      catch (Exception $e) {
          $response["msg"] =  $e->getMessage();
          $response["status"] = "Failed";
          $response["is_success"] = false;
      }
      return $response;
    }

    //DELETE A CONTENT
    public function destroy($id)
    {
        $response = array('status' => 'Failed','msg' => '', 'is_success'=>false);

      try {
          if ($id == 0 || $id == "" || $id == null) {
              $response["msg"] = "Invalid ID";
              $response["status"] = "Failed";
              $response["is_success"] = false;
          }
          else
          {
              $content = Content::find($id);
              if($content->delete())
              {
                $response["msg"] = "Content deleted successfully";
                $response["status"] = "Success";
                $response["is_success"] = true;
              }
              else
              {
                $response["msg"] = "Operation failed. Please try again.";
                $response["status"] = "Failed";
                $response["is_success"] = false;
              }
          }
      }
      catch (Exception $e) {
          $response["msg"] =  $e->getMessage();
          $response["status"] = "Failed";
          $response["is_success"] = false;
      }
      return $response;
    }
}
