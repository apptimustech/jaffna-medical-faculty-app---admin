<?php
namespace App\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use App\Attachment;
use App\Content;
use App\UserImpression;
use Illuminate\Http\Request;
use Validator;
use DB;


class AttachmentController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
      //  dd(User::all());
       return Attachment::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function getOne($id)
    {
      $attachment = Attachment::find($id);
      return $attachment;
    }

    /** ADD NEW RESOURCE **/
    public function add(Request $request)
    {
        $response = array('status' => 'Failed','msg' => '', 'is_success'=>false);

        try {
            $rules = [
                    'content_id' => 'required',
                    
            ];

            $customMessages = [
                'content_id.required' => 'Content ID cannot be empty.'
            ];

            $validator = Validator::make($request->all(), $rules, $customMessages);
            if ($validator->fails()) {
                $response["msg"] = $validator->messages()->first();
                $response["status"] = "Failed";
                $response["is_success"] = false;
            }
            else
            {
                $filepath = '';
                 //Values(files) upload
                 if ($request->hasFile('file')) {
                    $image = $request->file('file');
                    $name = rand(1000000,10000000).time().'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/download');
                    $image->move($destinationPath, $name); 
                    $filepath =  $name;          
                }
               
                $attachment = new Attachment();
                //$attachment->name = $request->name;
                $attachment->type = $request->type;
                //$attachment->value = $request->value;
                $attachment->content_id = $request->content_id; 

                if($filepath != "")
                    $attachment->file = $filepath;
                else
                    $attachment->file = $request->name;



                if($attachment->save())
                {
                    $response["attachment"] = $attachment;
                    $response["msg"] = "Attachment added successfully";
                    $response["status"] = "Success";
                    $response["is_success"] = true;
                }
                else
                {
                    $response["msg"] = "Operation failed. Please try again.";
                    $response["status"] = "Failed";
                    $response["is_success"] = false;
                }
            }
        }
        catch (Exception $e) {
            $response["msg"] =  $e->getMessage();
            $response["status"] = "Failed";
            $response["is_success"] = false;
        }
        return $response;
    }

  /** UPDATE A RESOURCE **/
  public function update(Request $request, $id)
  {
      $response = array('status' => 'Failed','msg' => '', 'is_success'=>false);

      try {
          $rules = [
            //'type' => 'required|max:45',
            'file' => 'required|max:255',
            'content_id' => 'required',
          ];

          $customMessages = [
            //'type.required' => 'Type cannot be empty.',
            'file.required' => 'File cannot be empty.',
            'content_id.required' => 'Contents ID cannot be empty.'
          ];
          $validator = Validator::make($request->all(), $rules, $customMessages);
          if ($validator->fails()) {
              $response["msg"] = $validator->messages()->first();
              $response["status"] = "Failed";
              $response["is_success"] = false;
          }
          else
          {
              $attachment = Attachment::find($id);

              if($attachment != null)
              {
                $filepath = '';

                //Values(files) upload
                if ($request->hasFile('file')) {
                   $image = $request->file('file');
                   $name = time().'.'.$image->getClientOriginalExtension();
                   $destinationPath = public_path('/download');
                   $image->move($destinationPath, $name); 
                   $filepath =  $destinationPath.'/'.$name;          
               }
 

                $attachment->type = $request->type;
                $attachment->file = $filepath;
                $attachment->content_id = $request->content_id;

                if($attachment->save())
                {
                  $response["msg"] = "Attachment updated successfully";
                  $response["status"] = "Success";
                  $response["is_success"] = true;
                }
                else
                {
                  $response["msg"] = "Operation failed. Please try again.";
                  $response["status"] = "Failed";
                  $response["is_success"] = false;
                }
              }
              else
              {
                $response["msg"] = "Invalid ID";
                $response["status"] = "Failed";
                $response["is_success"] = false;
              }

          }
      }
      catch (Exception $e) {
          $response["msg"] =  $e->getMessage();
          $response["status"] = "Failed";
          $response["is_success"] = false;
      }
      return $response;
  }

  public function delete($id)
    {
        $response = array('status' => 'Failed','msg' => '', 'is_success'=>false);

        try {
            if ($id == 0 || $id == "" || $id == null) {
                $response["msg"] = "Invalid ID";
                $response["status"] = "Failed";
                $response["is_success"] = false;
            }
            else
            {
                $attachment = Attachment::find($id);
                if ($attachment == null) {
                  $response["msg"] = "Operation failed. Please try again.";
                  $response["status"] = "Failed";
                  $response["is_success"] = false;
                }
                elseif($attachment->delete())
                {
                  $response["msg"] = "attachment deleted successfully";
                  $response["status"] = "Success";
                  $response["is_success"] = true;
                }
                else
                {
                  $response["msg"] = "Operation failed. Please try again.";
                  $response["status"] = "Failed";
                  $response["is_success"] = false;
                }
            }
        }
        catch (Exception $e) {
            $response["msg"] =  $e->getMessage();
            $response["status"] = "Failed";
            $response["is_success"] = false;
        }
        return $response;
    }      



    /*public function getDownload($id)
    {   
        $response = array('status' => 'Failed','msg' => '', 'is_success'=>false);

        try 
        {
            $validator = Validator::make($request->all(), ['file'   => 'mimes:doc,pdf,docx,zip']);
            if ($validator->fails())
            {
                $response["msg"] = $validator->messages()->first();
                $response["status"] = "Failed";
                $response["is_success"] = false;
            }

            else
            {
                $attachment = Attachment::find($id);

                if($attachment != null)
                {
                //PDF is stored in project/public/download/info.pdf
                $file= public_path()."/download";
                $headers= ['Content-Type'=> 'application/pdf',];
                $data=response()-> download ($file, $attachment->value, $headers);

                $response["msg"] = "Content found ";
                $response["status"] = "Success";
                $response["is_success"] = true;
                $response["data"] = $file;
                }
            }
        }

        catch (Exception $e) 
        {
            $response["msg"] =  $e->getMessage();
            $response["status"] = "Failed";
            $response["is_success"] = false;
        }
        return  $response;
        
    }*/

    public function getDownload($id)
    {   
        try 
        {
            $userImpression = new UserImpression;
            $userImpression->id_type = 'attachment_id';
            $userImpression->id_value = $id;
            $userImpression->type = 'download';
            $userImpression->ip = request()->ip();
            $userImpression->value = 1;
            $userImpression->save();

            $count = UserImpression::where('id_value','=',$id)->where('type','=','download')->sum('value');
            $response['data'] = ['downloads'=>$count];
            $attachment = Attachment::find($id);

            if($attachment != null)
            {
            $data = Attachment::find($id);

            //PDF is stored in project/public/download/info.pdf
            $file= $data->value;
            $headers= ['Content-Type'=> 'application/'.$data->type];

            return response()-> download ($file);
            }

            else
            {
                $response["msg"] = "Content file not found ";
                $response["status"] = "Failed";
                $response["is_success"] = false;
                //$response["data"] = $out;
            }
        }

    catch (Exception $e) 
        {
            $response["msg"] =  $e->getMessage();
            $response["status"] = "Content file not found ";
            $response["is_success"] = false;
        }
        return  $response;
      }
}
