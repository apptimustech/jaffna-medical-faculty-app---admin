<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function login(Request $request)
    {
        
    try {
        // validate the info, create rules for the inputs
        $rules = 
            [
                    'user_name' => 'required|unique:users|max:45',
                    'password' => 'required|max:200|min:6',
                   
            ];

            $customMessages =
            [  
                'user_name.required' => 'User name cannot be empty',
                'password.required' => 'Password cannot be empty.',
            ];

          $validator = Validator::make($request->all(), $rules, $customMessages);
            if ($validator->fails()) 
            {
                $response["msg"] = $validator->messages()->first();
                $response["status"] = "Failed";
                $response["is_success"] = false;
            }

            else 
            {
                // validation successful!
                // redirect them to the secure section or whatever
                // return Redirect::to('secure');
                $user = new User();
                $newToken = str_random(30);
                $user->token = $newToken;
                $response["msg"] = "User logged in successfully";
            }
            //return $response;
        }
        catch (Exception $e)
        {
            $response["msg"] =  $e->getMessage();
            $response["status"] = "Failed";
            $response["is_success"] = false;
        }
    }
}