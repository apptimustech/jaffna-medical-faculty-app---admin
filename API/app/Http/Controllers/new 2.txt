 public function resetPassword(){
        if(Auth::attempt(['user_name' => request('user_name'), 'password' => request('password'),'is_deleted'=>0])){
            {
            $user = Auth::user();
            //$success['remember_token'] = $user->createToken('MyApp')->accessToken;
            $response["msg"] = 'Successfully logged into the system';
            $response["status"] = "success";
            $response["is_success"] = true;
            $response["remember_token"] = $user->createToken('MyApp')->accessToken;

            return $response;
            }
            
        }else{
            $response["msg"] = 'User name and password are not matching';
              $response["status"] = "Failed";
              $response["is_success"] = false;
            return $response;
        }
    }