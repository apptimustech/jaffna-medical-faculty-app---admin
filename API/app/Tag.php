<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Content;

class Tag extends Model
{
    public function contents()
     {
            return $this->belongsToMany('App\Content','contents_has_tags','contents_id','tags_id');
     }
}
