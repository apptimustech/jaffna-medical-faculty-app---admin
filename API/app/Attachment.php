<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    public function content()
    {
        return $this->belongsTo('App\Content', 'content_id');
    }
}
