<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Content;

class Category extends Model
{
    public function children()
  {
      return $this->hasMany(self::class, 'parent_id');
  }

  public function parent()
  {
      return $this->belongsTo(self::class, 'parent_id');
  }

  public function content()
  {
      return $this->hasMany('App\Content'); // This only gets the contents of the current category
  }
}
