<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Attachment;
use App\UserImpression;
use App\Tag;
use App\Category;

class Content extends Model
{
    public function category()
    {
        return $this->belongsTo('App\Category', 'Category_id');
    }

    public function tags()
     {
            return $this->belongsToMany('App\Tag','content_has_tags','contents_id','tags_id');
     }
     
    public function userImpressions()
     {
         return $this->hasMany('App\UserImpression');
     }

     public function attachments()
     {
         return $this->hasMany('App\Attachment');
     }
}
