<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tag;
use App\Content;

class ContentHasTag extends Model
{
    public function content()
    {
        return $this->belongsTo('App\Content', 'contents_id');
    }

    public function tag()
    {
        return $this->belongsTo('App\Tag', 'tags_id');
    }
}
