<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserImpressionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_impressions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->string('value');
            //$table->dateTime('date_time');
            $table->string('ip',100);
            $table->integer('content_id')->unsigned();
            $table->timestamps();

            $table->index("content_id", 'fk_user_impressions_user_impressions1_idx');


            $table->foreign('content_id')
                ->references('id')->on('contents')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_impressions');
    }
}
