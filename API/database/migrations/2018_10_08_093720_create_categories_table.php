<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->longText('description')->nullable();
            $table->longText('bg')->nullable();
            $table->integer('parent_id')->unsigned()->nullable();

            $table->index('parent_id','fk_product_catagories_product_catagories1_idx');

            $table->foreign('parent_id')
                ->references('id')->on('categories')
                ->onDelete('set null')
                ->onUpdate('no action');
      
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
