<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->longText('body')->nullable();
            $table->integer('category_id')->unsigned()->nullable();

           // $table->index('category_id','fk_product_catagories_product_catagori_idx');
            $table->index("category_id", 'fk_contents_categories1_idx');
            $table->foreign('category_id','fk_contents_categories1_idx')
                ->references('id')->on('categories')
                ->onDelete('set null')
                ->onUpdate('no action');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}
